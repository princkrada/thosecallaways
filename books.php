<?php
/**
 * Template Name: Books
 */

get_header(); ?>
<link rel='stylesheet' href='<?php bloginfo('template_url'); ?>/inc/3rd-party/unitegallery/css/unite-gallery.css' type='text/css' />
<link rel='stylesheet'        href='<?php bloginfo('template_url'); ?>/inc/3rd-party/unitegallery/themes/default/ug-theme-default.css' type='text/css' />

<?php require_once( get_stylesheet_directory() . '/inc/book-header.php' ); ?>

<!-- <section id="featured-posts">
  <div class="container">
    <h2 class="text-center">Featured Posts</h2>
    <?php
    $query_args = array(
      'post_type'       => 'post',
      'category' => 15,
      'order' => asc,
      'posts_per_page' => '3'
    );

    global $post;
    $count = 0;
    $book_blog = get_posts( $query_args );
    foreach($book_blog as $post) : setup_postdata($post);

     ?>
    <div class="col-md-4">
      <h4><?= $post->post_title?></h4>
      <p><?= get_post_meta($post->ID, the_excerpt(), TRUE) ?></p>
    </div>
    <?php
	   endforeach;
	   wp_reset_postdata();
	    ?>
  </div>
</section>
<section id="check-today">
  <div class="container">
    <div class="col-md-8">
      <div class="col-md-6">
        <div class="media">
          <div class="media-left">
            <a href="#">
              <img src="<?php bloginfo('template_url'); ?>/images/clientsfirst.png" width="100">
            </a>
          </div>
          <div class="media-body">
            <h4 class="media-heading">Media heading</h4>
            <p>Super Agent is an agent guide - Always know what you are doing. Super Agent is an agent handbook - All the lessons are there, Super Agent is an Agentís Bible - Filled with daily inspirations,Super Agent is the one book every real estate agent should own. Get Yours Super Agent book Today and Reveal the five superpowers</p>
          </div>
        </div>
      </div>
      <div class="col-md-6">
        <div class="media">
          <div class="media-left">
            <a href="#">
              <img src="<?php bloginfo('template_url'); ?>/images/clientsfirst.png" width="100">
            </a>
          </div>
          <div class="media-body">
            <h4 class="media-heading">Media heading</h4>
            <p>Super Agent is an agent guide - Always know what you are doing. Super Agent is an agent handbook - All the lessons are there, Super Agent is an Agentís Bible - Filled with daily inspirations,Super Agent is the one book every real estate agent should own. Get Yours Super Agent book Today and Reveal the five superpowers</p>
          </div>
        </div>
      </div>
    </div>
    <div class="col-md-4" style="background-color: #19426D; padding:30px;">
      <h2 style="color: white;text-transform: uppercase;text-align: center">Check Out Today</h2>
    </div>
  </div>
</section> -->
<div class="container pages-container book-page">
  <div class="row">
    <div class="col-md-9">
      <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
      <div class="page-content book">
        <h2>FEATURED POSTS</h2>
        <?php
        $query_args = array(
          'post_type'       => 'post',
          'category' => 1036,
          'order' => asc,
          'posts_per_page' => '3'
        );

        global $post;
        $count = 0;
        $book_blog = get_posts( $query_args );
        foreach($book_blog as $post) : setup_postdata($post);

         ?>

          <h4><a href="<?= get_post_meta($post, the_permalink(), TRUE) ?>"><?= $post->post_title?></a></h4>
          <p><?= get_post_meta($post, the_excerpt(), TRUE) ?></p>
          <p class="small">
            <?php the_time('F jS, Y') ?> &nbsp;|&nbsp;
            by <?php the_author() ?> &nbsp;|&nbsp;
            Published in
            <?php the_category(', ');
              if($post->comment_count > 0) {
                  echo ' &nbsp;|&nbsp; ';
                  comments_popup_link('', '1 Comment', '% Comments');
              }
            ?>
          </p>

        <?php
    	   endforeach;
    	   wp_reset_postdata();
    	    ?>
          <hr>
          <h2>CHECK OUT TODAY</h2>
          <div class="media">
            <div class="media-left">
              <a href="<?= home_url()?>/shop">
                <img src="<?php bloginfo('template_url'); ?>/images/clientsfirst.png" width="65">
              </a>
            </div>
            <div class="media-body comment-blog">
              <p>Super Agent is an agent guide - Always know what you are doing. Super Agent is an agent handbook - All the lessons are there, Super Agent is an Agentís Bible - Filled with daily inspirations,Super Agent is the one book every real estate agent should own. Get Yours Super Agent book Today and Reveal the five superpowers </p>
            </div>
          </div>
          <div class="media">
            <div class="media-left">
              <a href="<?= home_url()?>/shop">
                <img src="<?php bloginfo('template_url'); ?>/images/clientsfirst.png" width="65">
              </a>
            </div>
            <div class="media-body comment-blog">
              <p>Now in its 3rd printing New York Times Best Seller and Wall Street Journal Number One Business Book. Unlock your potential, and life’s rewards, by putting Clients First</p>
            </div>
          </div>
          <hr>
          <?php
          if ( comments_open() || get_comments_number() ) {

            comments_template();
          }

          ?>
      </div>
      <?php
      endwhile;
      else:
      endif;
      ?>
    </div>

    <div class="col-md-3 side-bar-right">
    <?php if(is_active_sidebar('books-sidebar-widgets')){ dynamic_sidebar('books-sidebar-widgets');}?>
    </div><!-- col-md-4 -->
  </div>
</div>
<script>
jQuery(function(){
  jQuery("#gallery").unitegallery({
    gallery_theme: "slider",
    gallery_carousel:false,
    gallery_control_thumbs_mousewheel:false,
    slider_enable_bullets: false,
    slider_enable_arrows: false
});
});

</script>
<script type='text/javascript' src='<?php bloginfo('template_url'); ?>/inc/3rd-party/unitegallery/js/unitegallery.min.js'></script>
<script type='text/javascript' src='<?php bloginfo('template_url'); ?>/inc/3rd-party/unitegallery/themes/default/ug-theme-default.js'></script>
<script src='<?php bloginfo('template_url'); ?>/inc/3rd-party/unitegallery/themes/slider/ug-theme-slider.js' type='text/javascript'></script>

<?php get_footer(); ?>
