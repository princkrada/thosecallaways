<?php
/**
 * Template Name: Home Page
 *
 */

get_header();
?>
<!-- Full Page Image Background Carousel Header -->
<header id="myCarousel" class="carousel slide">
	<!-- Indicators -->
	<ol class="carousel-indicators">
		<li data-target="#myCarousel" data-slide-to="0" class="active"></li>
		<li data-target="#myCarousel" data-slide-to="1"></li>
		<li data-target="#myCarousel" data-slide-to="2"></li>
	</ol>
	<!-- Wrapper for Slides -->
	<div class="carousel-inner">

		<?php

		$slider = get_field('slider');

	//	var_dump($slider['slider_image']);
foreach ($slider as $slide):
    $count       = $count + 1;
    $first_class = (1 == $count) ? 'active' : '';
?>
		<div class="item <?= $first_class ?>">
			<!-- Set the first background image using inline CSS below. -->
			<div class="fill" style="background-image: url(<?=$slide['slider_image']['url']?>)"></div>
			<div class="container">
				<div class="row slide-margin">
					<div class="col-sm-5">
							<div class="carousel-content">
								<div class="carousel-inside">
									<a href="<?=$slide['property_link']?>">
										<?=$slide['property_name']?>
									</a>
								</div>
							</div>
						</div>
						<div class="col-sm-7">
							<?php
							if($slide['sign-up_for_newsletter'] ==  'Enable'){
								?>
							<div class="carousel-content">
								<div class="alert alert-subscribe text-center wow fadeIn">
									<a class="close" href="#" data-dismiss="alert">×</a>
									<?= do_shortcode('[gravityform id="2" title="true" description="true"]');?>
								</div>
							</div>
							<?php } else{}?>
						</div>
				</div>
			</div>
		</div><!-- end item -->
		<?php
	endforeach;
?>
</div>
<!-- Controls -->
<a class="left carousel-control" href="#myCarousel" data-slide="prev">
	<span class="icon-prev"></span>
</a>
<a class="right carousel-control" href="#myCarousel" data-slide="next">
	<span class="icon-next"></span>
</a>
</header>
<section id="search-filters">
	<!-- SEARCH BY CITY -->
	<div class="container">
		<div class="row">
			<div class="col-lg-12">
				<form method="get" class="listing-search-form IDX-searchForm IDX-searchForm-search  IDX-activept-sfr" action="http://thosecallaways.idxbroker.com/idx/results/listings">
   <div class="row">
      <div class="form-group col-sm-3">
         <input type="text" class="form-control"  placeholder="MLS #" name="csv_listingID">
      </div>
      <div class="form-group col-sm-3">
         <!-- <input type="text" class="form-control"  placeholder="City" name="city"> -->
         <select id="IDX-city" name="city[]" class="IDX-select IDX-cczSelect form-control">
            <option value="">Select City</option>
            <option value="362">Aguila</option>
            <option value="54011">Ahwatukee</option>
            <option value="392">Ajo</option>
            <option value="808">Alpine</option>
            <option value="1014">Amado</option>
            <option value="1314">Anthem</option>
            <option value="1358">Apache Junction</option>
            <option value="1565">Arivaca</option>
            <option value="1567">Arizona City</option>
            <option value="1587">Arlington</option>
            <option value="1751">Ash Fork</option>
            <option value="2203">Avondale</option>
            <option value="2291">Bagdad</option>
            <option value="3417">Bellemont</option>
            <option value="3641">Benson</option>
            <option value="4191">Bisbee</option>
            <option value="4246">Black Canyon City</option>
            <option value="5108">Bouse</option>
            <option value="5147">Bowie</option>
            <option value="6143">Buckeye</option>
            <option value="6295">Bullhead City</option>
            <option value="6978">Camp Verde</option>
            <option value="7279">Carefree</option>
            <option value="7538">Casa Grande</option>
            <option value="7719">Cave Creek</option>
            <option value="8166">Chandler</option>
            <option value="8656">Chino Valley</option>
            <option value="8800">Circle City</option>
            <option value="8925">Clarkdale</option>
            <option value="9026">Clay Springs</option>
            <option value="9033">Claypool</option>
            <option value="9218">Clifton</option>
            <option value="9475">Cochise</option>
            <option value="9973">Concho</option>
            <option value="10039">Congress</option>
            <option value="10154">Coolidge</option>
            <option value="10270">Cordes Lakes</option>
            <option value="10350">Cornville</option>
            <option value="10487">Cottonwood</option>
            <option value="11013">Crown King</option>
            <option value="11562">Dateland</option>
            <option value="12152">Desert Hills</option>
            <option value="12205">Dewey</option>
            <option value="12617">Douglas</option>
            <option value="12715">Dragoon</option>
            <option value="12943">Duncan</option>
            <option value="13145">Eagar</option>
            <option value="14073">El Mirage</option>
            <option value="14184">Elfrida</option>
            <option value="14186">Elgin</option>
            <option value="14507">Eloy</option>
            <option value="16096">Flagstaff</option>
            <option value="16207">Florence</option>
            <option value="16409">Forest Lakes</option>
            <option value="16590">Fort McDowell</option>
            <option value="16601">Fort Mohave</option>
            <option value="16746">Fountain Hills</option>
            <option value="16974">Fredonia</option>
            <option value="17868">Gila Bend</option>
            <option value="17872">Gilbert</option>
            <option value="18135">Glendale</option>
            <option value="18253">Globe</option>
            <option value="18313">Gold Canyon</option>
            <option value="18350">Golden Valley</option>
            <option value="18463">Goodyear</option>
            <option value="19037">Green Valley</option>
            <option value="19092">Greenehaven</option>
            <option value="19240">Greer</option>
            <option value="19417">Guadalupe</option>
            <option value="19604">Hackberry</option>
            <option value="20018">Happy Jack</option>
            <option value="20678">Heber</option>
            <option value="20906">Hereford</option>
            <option value="21490">Holbrook</option>
            <option value="22042">Huachuca City</option>
            <option value="22159">Humboldt</option>
            <option value="23537">Joseph City</option>
            <option value="23788">Kearny</option>
            <option value="24281">Kingman</option>
            <option value="24421">Kirkland</option>
            <option value="25093">Lake Havasu City</option>
            <option value="25298">Lakeside</option>
            <option value="25757">Laveen</option>
            <option value="26685">Litchfield Park</option>
            <option value="28074">Mammoth</option>
            <option value="28402">Marana</option>
            <option value="28489">Maricopa</option>
            <option value="28996">Mayer</option>
            <option value="29411">McNeal</option>
            <option value="29476">Meadview</option>
            <option value="29835">Mesa</option>
            <option value="29919">Miami</option>
            <option value="30656">Mobile</option>
            <option value="30704">Mohave Valley</option>
            <option value="31203">Mormon Lake</option>
            <option value="31257">Morristown</option>
            <option value="31800">Munds Park</option>
            <option value="32649">New River</option>
            <option value="33083">Nogales</option>
            <option value="33820">Nutrioso</option>
            <option value="34587">Oracle</option>
            <option value="34741">Oro Valley</option>
            <option value="35">Other</option>
            <option value="12">Other</option>
            <option value="4">Other</option>
            <option value="34969">Overgaard</option>
            <option value="35284">Palo Verde</option>
            <option value="35382">Paradise Valley</option>
            <option value="35524">Parker</option>
            <option value="35564">Parks</option>
            <option value="35721">Patagonia</option>
            <option value="35794">Paulden</option>
            <option value="35857">Payson</option>
            <option value="35876">Peach Springs</option>
            <option value="35894">Pearce</option>
            <option value="35960">Peeples Valley</option>
            <option value="36139">Peoria</option>
            <option value="36412">Phoenix</option>
            <option value="36426">Picacho</option>
            <option value="36559">Pima</option>
            <option value="36569">Pine</option>
            <option value="36673">Pinedale</option>
            <option value="36704">Pinetop</option>
            <option value="37490">Portal</option>
            <option value="37787">Prescott</option>
            <option value="37793">Prescott Valley</option>
            <option value="38173">Quartzsite</option>
            <option value="38191">Queen Creek</option>
            <option value="38192">Queen Valley</option>
            <option value="38723">Red Rock</option>
            <option value="39404">Rimrock</option>
            <option value="39465">Rio Rico</option>
            <option value="39467">Rio Verde</option>
            <option value="40027">Roll</option>
            <option value="40096">Roosevelt</option>
            <option value="40728">Saddlebrooke</option>
            <option value="40741">Safford</option>
            <option value="40766">Sahuarita</option>
            <option value="40837">Saint David</option>
            <option value="41133">Salome</option>
            <option value="41266">San Manuel</option>
            <option value="41298">San Simon</option>
            <option value="54010">San Tan Valley</option>
            <option value="41348">Sanders</option>
            <option value="41877">Scottsdale</option>
            <option value="42096">Sedona</option>
            <option value="42130">Seligman</option>
            <option value="42814">Show Low</option>
            <option value="42836">Shumway</option>
            <option value="42894">Sierra Vista</option>
            <option value="43133">Skull Valley</option>
            <option value="43365">Snowflake</option>
            <option value="44208">Spring Valley</option>
            <option value="44246">Springerville</option>
            <option value="56748">St Johns</option>
            <option value="44407">Stanfield</option>
            <option value="57240">Star Valley</option>
            <option value="44993">Strawberry</option>
            <option value="45293">Sun City</option>
            <option value="45300">Sun City West</option>
            <option value="45301">Sun Lakes</option>
            <option value="45304">Sun Valley</option>
            <option value="45409">Superior</option>
            <option value="45433">Surprise</option>
            <option value="45696">Tacna</option>
            <option value="45906">Taylor</option>
            <option value="45998">Tempe</option>
            <option value="46132">Thatcher</option>
            <option value="46587">Tolleson</option>
            <option value="46600">Toltec</option>
            <option value="46618">Tombstone</option>
            <option value="46639">Tonopah</option>
            <option value="46642">Tonto Basin</option>
            <option value="46670">Topock</option>
            <option value="46705">Tortilla Flat</option>
            <option value="47040">Truxton</option>
            <option value="47051">Tubac</option>
            <option value="47065">Tucson</option>
            <option value="48570">Vail</option>
            <option value="48666">Valley Farms</option>
            <option value="48941">Vernon</option>
            <option value="49532">Waddell</option>
            <option value="50575">Wellton</option>
            <option value="50595">Wenden</option>
            <option value="51536">White Mountain Lake</option>
            <option value="51735">Wickenburg</option>
            <option value="51765">Wikieup</option>
            <option value="51863">Willcox</option>
            <option value="51873">Williams</option>
            <option value="52243">Winkelman</option>
            <option value="52281">Winslow</option>
            <option value="52384">Wittmann</option>
            <option value="52874">Yarnell</option>
            <option value="52979">Young</option>
            <option value="53004">Youngtown</option>
            <option value="53015">Yucca</option>
            <option value="53024">Yuma</option>
         </select>
      </div>


      <div class="form-group col-sm-3">
         <select id="IDX-pt" name="pt" class="IDX-select IDX-searchType-m form-control" autocomplete="off">
            <option value="">Select Type</option>
            <option value="sfr">Single Family Residential</option>
            <option value="rnt">Rentals</option>
            <option value="ld">Lots and Land</option>
            <option value="com">Commercial</option>
            <option value="mfr">Multiple Dwellings</option>
         </select>
         <!-- <select id="IDX-pt" name="pt" class="IDX-select IDX-searchType-m form-control" autocomplete="off">
            <option value="sfr">Single Family Residential</option>
            <option value="rnt">Rentals</option>
            <option value="ld">Lots and Land</option>
            <option value="com">Commercial</option>
            <option value="mfr">Multiple Dwellings</option>
         </select> -->
      </div>
      <div class="form-group col-sm-3">
         <select id="IDX-a_propStatus" name="a_propStatus[]" class="IDX-select form-control">
            <option value="" selected>Status</option>
            <option value="Active">Active</option>
            <!-- <option value="Featured">Featured</option>
            <option value="For Rent">For Rent</option>
            <option value="New">New</option>
            <option value="Pending">Pending</option>
            <option value="Reduced">Reduced</option>
            <option value="Sold">Sold</option> -->
         </select>
      </div>
   </div>
   <div class="row" style="margin-top: 20px;">
      <div class="form-group col-sm-2">
         <!-- <input type="text" class="form-control"  placeholder="Bedrooms" name="bd"> -->
          <select id="IDX-bd" name="bd" class="IDX-select form-control" autocomplete="off">
             <option value="">Bedrooms</option>
             <option value="0">Studio</option>
             <option value="1">1+</option>
             <option value="2">2+</option>
             <option value="3">3+</option>
             <option value="4">4+</option>
             <option value="5">5+</option>
             <option value="6">6+</option>
             <option value="7">7+</option>
             <option value="8">8+</option>
             <option value="9">9+</option>
             <option value="10">10+</option>
          </select>
      </div>
      <div class="form-group col-sm-2">
         <!-- <input type="text" class="form-control"  placeholder="Bathrooms" name="tb"> -->
         <select id="IDX-tb" name="tb" class="IDX-select form-control" autocomplete="off">
             <option value="">Bathrooms</option>
             <option value="1">1+</option>
             <option value="2">2+</option>
             <option value="3">3+</option>
             <option value="4">4+</option>
             <option value="5">5+</option>
             <option value="6">6+</option>
             <option value="7">7+</option>
             <option value="8">8+</option>
             <option value="9">9+</option>
             <option value="10">10+</option>
          </select>
      </div>
      <div class="form-group col-sm-2">
         <input type="text" class="form-control"  placeholder="Minimum Price" name="lp">
      </div>
      <div class="form-group col-sm-2">
         <input type="text" class="form-control"  placeholder="Maximum Price" name="hp">
      </div>
      <div class="form-group col-sm-2">
      </div>
   </div>
   <!-- <button type="submit" id="IDX-formSubmit" class="btn-tc btn btn-default IDX-formBtn IDX-formSubmit">SUBMIT</button> -->
   <button type="submit" id="IDX-formSubmit-bottom" class="IDX-formBtn IDX-formSubmit btn-tc btn btn-default">SEARCH</button>
   <a href="http://thosecallaways.idxbroker.com/idx/searchbycity" class="btn-tc btn btn-default">OR SEARCH BY CITY</a>
</form>
			</div>
		</div>
	</div>
	<!-- /.container -->
</section>
<section id="zillow" class="latest-blogs" style="background-image: url(<?php
bloginfo('template_url');
?>/images/zillow.jpg); background-size: cover;">
	<div class="container text-center" >
		<img src="<?php
bloginfo('template_url');
?>/images/verified-zillow.png" class="verified-img">
		<div class="row" style="margin-bottom: 48px">
			<h3>THOSE CALLAWAYS</h3>
			<a href="http://www.zillow.com/profile/Those-Callaways/" target="_blank"><img src="<?php
bloginfo('template_url');
?>/images/zillow-txt.png"></a>
			<h3>BY THE NUMBERS</h3>
			<div class="counters">

			<?php
$number_of_properties = get_field('number_of_properties_sold');
$rating               = get_field('rating');
$number_of_reviews    = get_field('number_of_reviews');
?>

				<div class="col-md-4 col-sm-4 col-xs-12 text-center">
					<span class="counter circle"><?= $number_of_properties; ?></span>
					<h4># of properties sold</h4>
				</div>
				<div class="col-md-4 col-sm-4 col-xs-12 text-center">
					<span class="counter circle"><?= $rating; ?></span>
					<h4>out of 5 star rating</h4>
				</div>
				<div class="col-md-4 col-sm-4 col-xs-12 text-center">
					<span class="counter circle"><?= $number_of_reviews; ?></span>
					<h4># of reviews</h4>
				</div>
			</div>
		</div>
	</div>
	<div class="col-sm-12 line text-center">
		<a href="http://www.zillow.com/profile/Those-Callaways/" target="_blank"><span class="bottom-text">View listings and reviews on</span><img src="<?php
bloginfo('template_url');
?>/images/zillow-yellow.png" class="z-small"></a>
	</div>
</section>
<!-- Feautured Properties Section -->
<section id="featured-properties">
	<div class="container">
		<div class="row sec-title">
			<div class="col-lg-12 text-center">
				<h1 class="wow fadeIn">Featured Properties</h1>
			</div>
		</div>
		<div class="row">
			<?= do_shortcode('[featured-listings]') ?>
			<a href="http://thosecallaways.idxbroker.com/idx/results/listings" class="btn btn-secondary center-block">VIEW MORE</a>
		</div>
	</div>
</section>
<section id="image-buttons" class="" style="background-image: url(<?php
bloginfo('template_url');
?>/images/image-buttons.jpg); background-size: cover;background-repeat: no-repeat;background-attachment: fixed;">

	<div class="container">
		<div class="row">
			<?php
			$boxes = get_field('box_image');

			foreach ($boxes as $box):
				?>
			<div class="col-md-4 col-sm-4">
				<div class="hovereffect">
					<div class="image-container animated wow fadeInUp">
						<img src="<?=$box['box_image']['url']?>">
						<div class="after">
							<a href="<?=$box['box_link']?>">
								<p><?=$box['box_label']?></p>
							</a>
						</div>
					</div>
				</div>
			</div>
			<?php endforeach;
			 ?>


		</div>
	</div>
</section>
<section id="comments">
	<!-- SEARCH BY CITY -->
	<div class="container">
		<div class="row">
			<div class="col-lg-12 wow fadeInUp" data-wow-duration="4s">
				<?= do_shortcode('[gravityform id="3" title="false" description="true"]') ?>
			</div>
		</div>
	</div>
	<!-- /.container -->
</section>
<?php
get_footer();
?>
