<?php
/**
 * The Template for displaying product archives, including the main shop page which is a post type archive
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/archive-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see 	    https://docs.woocommerce.com/document/template-structure/
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     2.0.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

get_header( 'shop' ); ?>
	<link rel='stylesheet' href='<?php bloginfo('template_url'); ?>/inc/3rd-party/unitegallery/css/unite-gallery.css' type='text/css' />
	<link rel='stylesheet'        href='<?php bloginfo('template_url'); ?>/inc/3rd-party/unitegallery/themes/default/ug-theme-default.css' type='text/css' />

	<?php require_once( get_stylesheet_directory() . '/inc/book-header.php' ); ?>
	<div class="container pages-container book-page">
		<div class="col-md-9">
			<?php
				/**
				 * woocommerce_before_main_content hook.
				 *
				 * @hooked woocommerce_output_content_wrapper - 10 (outputs opening divs for the content)
				 * @hooked woocommerce_breadcrumb - 20
				 */
				do_action( 'woocommerce_before_main_content' );
			?>

				

				<?php if ( apply_filters( 'woocommerce_show_page_title', true ) ) : ?>

					<h1 class="page-title"><?php woocommerce_page_title(); ?></h1>

				<?php endif; ?>

				<?php
					/**
					 * woocommerce_archive_description hook.
					 *
					 * @hooked woocommerce_taxonomy_archive_description - 10
					 * @hooked woocommerce_product_archive_description - 10
					 */
					do_action( 'woocommerce_archive_description' );
				?>

				<?php if ( have_posts() ) : ?>

					<?php
						/**
						 * woocommerce_before_shop_loop hook.
						 *
						 * @hooked woocommerce_result_count - 20
						 * @hooked woocommerce_catalog_ordering - 30
						 */
						do_action( 'woocommerce_before_shop_loop' );
					?>

					<?php woocommerce_product_loop_start(); ?>

						<?php woocommerce_product_subcategories(); ?>

						<?php while ( have_posts() ) : the_post(); ?>

							<?php wc_get_template_part( 'content', 'product' ); ?>

						<?php endwhile; // end of the loop. ?>

					<?php woocommerce_product_loop_end(); ?>

					<?php
						/**
						 * woocommerce_after_shop_loop hook.
						 *
						 * @hooked woocommerce_pagination - 10
						 */
						do_action( 'woocommerce_after_shop_loop' );
					?>

				<?php elseif ( ! woocommerce_product_subcategories( array( 'before' => woocommerce_product_loop_start( false ), 'after' => woocommerce_product_loop_end( false ) ) ) ) : ?>

					<?php wc_get_template( 'loop/no-products-found.php' ); ?>

				<?php endif; ?>

			<?php
				/**
				 * woocommerce_after_main_content hook.
				 *
				 * @hooked woocommerce_output_content_wrapper_end - 10 (outputs closing divs for the content)
				 */
				do_action( 'woocommerce_after_main_content' );
			?>
			</div>
			<div class="col-md-3 side-bar-right">
        <?php if(is_active_sidebar('books-sidebar-widgets')){ dynamic_sidebar('books-sidebar-widgets');}?>
      </div><!-- col-md-4 -->
</div>
<script>
jQuery(function(){
  jQuery("#gallery").unitegallery({
    gallery_theme: "slider",
    gallery_carousel:false,
    gallery_control_thumbs_mousewheel:false,
    slider_enable_bullets: false,
    slider_enable_arrows: false
});
});

</script>
<script type='text/javascript' src='<?php bloginfo('template_url'); ?>/inc/3rd-party/unitegallery/js/unitegallery.min.js'></script>
<script type='text/javascript' src='<?php bloginfo('template_url'); ?>/inc/3rd-party/unitegallery/themes/default/ug-theme-default.js'></script>
<script src='<?php bloginfo('template_url'); ?>/inc/3rd-party/unitegallery/themes/slider/ug-theme-slider.js' type='text/javascript'></script>

<?php get_footer( 'shop' ); ?>
