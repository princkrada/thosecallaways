	<footer>
		<section id="connect-with-us">
			<div class="container">
				<div class="row">
					<div class="col-lg-12 text-center wow fadeInUp" data-wow-duration="4s">
						<h1><span class="connect">CONNECT</span> <span class="with-us">WITH US</span></h1>
						<?= do_shortcode('[gravityform id="1" title="false" description="true"]') ?>
					</div>
				</div>
			</div>
		</section>
		<section id="bottom-footer">
			<div class="container">
				<div class="row ">
					<div class="col-md-3">
						<a href="/~thosecallaways"><img src="<?php bloginfo('template_url'); ?>/images/logo-footer.png" width="150" class="center-block"></a>
						<div style="clear:both"></div>
						<ul class="list-inline">
							<li>
								<a href="https://www.facebook.com/ThoseCallaways" class="btn-social btn-outline" target="_blank"><i class="fa fa-fw fa-facebook"></i></a>
							</li>
							<li>
								<a href="https://twitter.com/ThoseCallaways" class="btn-social btn-outline" target="_blank"><i class="fa fa-fw fa-twitter"></i></a>
							</li>
							<li>
								<a href="https://www.youtube.com/user/ThoseCallawaysVideos" class="btn-social btn-outline" target="_blank"><i class="fa fa-fw fa-youtube-play"></i></a>
							</li>
							<li>
								<a href="https://www.pinterest.com/thosecallaways/" class="btn-social btn-outline" target="_blank"><i class="fa fa-fw fa-pinterest"></i></a>
							</li>
							<li>
								<a href="https://plus.google.com/100571225885877826944" class="btn-social btn-outline" target="_blank"><i class="fa fa-fw fa-google-plus"></i></a>
							</li>
						</ul>
					</div>
					<div class="col-md-3">
						<h5>Those Callaways</h5>
						<p>You control every step with confidence that your decisions are carried out, deadlines are met, contegencies are staisfied, your desires are fulfilled and your goals become a reality.</p>
					</div>
					<div class="col-md-3">
						<h5>Office</h5>
						<p>We're always eager to help! <br>Feel free to contact us with any questions or concerns.</p>
						<p>Direct: <a href="tel:4805965751">480-596-5751</a><br>
						U.S.A: <a href="tel:18005965751">1-800-596-5751</a><br>
						Fax: <a href="tel:4804832947">480-483-2947</a></p>
						<p><a href="mailto:contactme@thosecallaways.com">contactme@thosecallaways.com</a></p>
					</div>
					<div class="col-md-3">
						<h5>Latest Blogs</h5>
						<?= do_shortcode('[latest_news]')?>
					</div>
				</div>
				<div class="col-lg-12 text-center" style="margin-top: 30px;">
					<span class="footer-pad">&copy; <a href="<?= home_url()?>">2016 Those Callaways</a></span><span class="footer-pad">|</span><span class="footer-pad"><a href="http://www.goodfellowcreative.com" target="_blank">Website by Goodfellow Creative</a></span>
				</div>
			</div>
		</section>
	</footer>
	<!-- <script	 src="https://code.jquery.com/jquery-3.0.0.min.js"	 integrity="sha256-JmvOoLtYsmqlsWxa7mDSLMwa6dZ9rrIdtrrVYRnDRH0="	 crossorigin="anonymous"></script> -->

	</body>
</html>
