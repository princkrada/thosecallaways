<?php // Do not delete these lines
	if ('comments.php' == basename($_SERVER['SCRIPT_FILENAME']))
		die ('Please do not load this page directly. Thanks!');
	if (!empty($post->post_password)) { // if there's a password
		if ($_COOKIE['wp-postpass_' . COOKIEHASH] != $post->post_password) {  // and it doesn't match the cookie
			?>

			<p class="nocomments">This post is password protected. Enter the password to view comments.</p>

			<?php
			return;
		}
	}
	/* This variable is for alternating comment background */
	$oddcomment = 'class="comments-alt" ';
?>

<!-- You can start editing here. -->

<?php if ($comments) : ?>

	<h2 id="comments"><?php comments_number('No Responses', 'RESPONSES', 'RESPONSES' );?></h2>

	<!-- <div class="small">
		<span class="feedlink"><?php comments_rss_link('Feed'); ?></span>
		<?php if ('open' == $post-> ping_status) { ?><span class="trackbacklink"><a href="<?php trackback_url() ?>" title="Copy this URI to trackback this entry.">Trackback Address</a></span><?php } ?>
	</div> -->

	

	<?php foreach ($comments as $comment) : ?>

    <div class="media">
      <div class="media-left">
        <a href="#">
          <?= get_avatar($comment->comment_id)?>
        </a>
      </div>
      <div class="media-body comment-blog">
        <h3><?= $comment->comment_author?></h3>
        <p class="small"><?php comment_date('F jS, Y') ?> at <?php comment_time() ?> <?php edit_comment_link('edit','&nbsp;&nbsp;',''); ?></p>
        <p><?= $comment->comment_content?></p>
      </div>
    </div>

	<?php
		/* Changes every other comment to a different class */
		$oddcomment = ( empty( $oddcomment ) ) ? 'class="comments-alt" ' : '';
	?>

	<?php endforeach; /* end for each comment */ ?>

	


 <?php else : // this is displayed if there are no comments so far ?>

	<?php if ('open' == $post->comment_status) : ?>
		<!-- If comments are open, but there are no comments. -->

	 <?php else : // comments are closed ?>
		<!-- If comments are closed. -->
		<p class="nocomments">Comments are closed.</p>

	<?php endif; ?>
<?php endif; ?>


<?php if ('open' == $post->comment_status) : ?>

<div class="message_heading">
  <h3 id="respond">Leave a Response</h3>
</div>
<?php if ( get_option('comment_registration') && !$user_ID ) : ?>
<p>You must be <a href="<?php echo get_option('siteurl'); ?>/wp-login.php?redirect_to=<?php the_permalink(); ?>">logged in</a> to post a comment.</p>
<?php else : ?>

<form action="<?php echo get_option('siteurl'); ?>/wp-comments-post.php" method="post" id="main-contact-form" class="contact-form" name="contact-form" role="form">

<?php if ( $user_ID ) : ?>

<p style="color: #333;">Logged in as <a href="<?php echo get_option('siteurl'); ?>/wp-admin/profile.php"><?php echo $user_identity; ?></a>. <a href="<?php echo get_option('siteurl'); ?>/wp-login.php?action=logout" title="Log out of this account">Logout &raquo;</a></p>

<?php else : ?>
<div class="row">
    <div class="col-sm-5">
        <div class="form-group">
            <label>Name *</label>
            <input type="text" class="form-control" required="required" name="author" id="author" value="<?php echo $comment_author; ?>">
        </div>
        <div class="form-group">
            <label>Email *</label>
            <input type="email" class="form-control" required="required" name="email" id="email" value="<?php echo $comment_author_email; ?>">
        </div>
        <div class="form-group">
            <label>URL</label>
            <input type="url" class="form-control" name="url" id="url" value="<?php echo $comment_author_url; ?>">
        </div>
    </div>

    <div class="col-sm-7">

        <div class="form-group">
            <label>Message *</label>
            <textarea name="comment" id="comment" required="required" class="form-control" rows="4"></textarea>
        </div>
        <div class="form-group">
            <!-- <button type="submit" class="btn btn-primary btn-lg" required="required">Submit Message</button> -->
            <input name="submit" type="submit" id="submit" tabindex="5" value="Submit Comment" />
        </div>
    </div>
</div>

<?php endif; ?>
<?php if ( $user_ID ) : ?>
	<div class="form-group">
			<label>Message *</label>
			<textarea name="comment" id="comment" required="required" class="form-control" rows="4"></textarea>
	</div>
	<div class="form-group">
			<!-- <button type="submit" class="btn btn-primary btn-lg" required="required">Submit Message</button> -->
			<input name="submit" type="submit" id="submit" tabindex="5" value="Submit Comment" />
	</div>
<?php else : ?>
<?php endif; ?>

<input type="hidden" name="comment_post_ID" value="<?php echo $id; ?>" />

<?php do_action('comment_form', $post->ID); ?>

</form>

<?php endif; // If registration required and not logged in ?>

<?php endif; // if you delete this the sky will fall on your head ?>
