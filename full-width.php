<?php
/**
 * Template Name: Full Width
 */

get_header(); ?>

<?php
    if ( has_post_thumbnail() ) {
        the_post_thumbnail();
    }
    else {
        echo '<img style="width: 100%; height: auto" src="'. get_bloginfo( 'stylesheet_directory' ).'/assets/img/interior-small.jpg" />';
    }


    global $wpdb;
    $result = $wpdb->get_results('SELECT * FROM ' . $wpdb->postmeta . ' WHERE post_id = 51');

    echo '<pre>';
    var_dump($result);
    echo '</pre>';
?>

<div class="container full-width" style="margin-top: 20px;">

  <div class="row">
    <div class="col-md-12">
      <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
      <div class="text-center page-pad">

        <h3 style="text-align: center"><?= the_title() ?></h3>
        <div style="margin-top: 50px;">
          <p><?= the_content() ?></p>
        </div>
      </div>
      <?php
      endwhile;
      else:
      endif;
      ?>
    </div>
  </div>
</div>

<?php get_footer(); ?>
