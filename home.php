<?php
/**
 *
 */

get_header();
global $post;
?>
<div style="height: 195px; background-image: url('<?= get_stylesheet_directory_uri()?>/images/3-full.jpg');background-size: cover;background-repeat: no-repeat; width: 100%;background-position:50% 10">
  <div class="ft-overlay">
    <div class="container">
      <span class="listing-detail-title"><h2>BLOG</h2></span>
    </div>
  </div>
</div>

<div class="container pages-container">
  <div class="row">
    <div class="col-md-9">
			<?php

      /*$cat_list = array(14,76,26,71,272, 267, 17,275,54,273, 48);*/

      if(!empty($_POST['check_list'])) {

        $args = array(
          'include' => $_POST['check_list']
        );
        /*$cats = $_POST['check_list'];
        $cats_checkbox = $cat_list;*/
        $number     = -1;

      }
      else{
        $args = array(
          'include' => $cat_list
        );
        $cats = $cat_list;
        $cats_checkbox = '';
        $number     = 5;
      }
      $categories = get_categories($args);


      $paged      = (get_query_var('paged')) ? get_query_var('paged') : 1;
      $offset     = ($paged - 1) * $number;
      $query_args = array(
          'post_type'       => 'post',
      		'posts_per_page' => $number,
      		'paged' => $paged,
          'category' => $cats

      );
      $post_array = get_posts( $query_args );

			$total_users = count($post_array);
			$total_query = count($query_args);
			$total_pages = intval($total_users / $number) + 1;
      ?>


      <div class="row-fluid">
      <div class="col-sm-6">

	      <div class="row center-block blog-checkbox">

		    <form id="category-select" class="category-select" action="<?php echo esc_url( home_url( '/' ) ); ?>" method="get">
		      <?php wp_dropdown_categories( 'show_count=0&hierarchical=1&class=form-control' ); ?>
		      <input type="submit" id="catsubmit" name="submit" value="Subject" />
		    </form>

	      </div>
      </div>

      <div class="col-sm-6">
      <form role="search" method="get" id="searchform" class="searchform blog-checkbox" action="<?= home_url()?>">
				<div>
					<label class="screen-reader-text" for="s">Search for:</label>
					<input type="text" value="" name="s" id="s">
					<input type="submit" id="searchsubmit" value="Search">
				</div>
	  </form>
	  </div>
	  </div>
      <div style="clear:both"></div>
      <hr>
      <?php
			foreach ( $post_array as $post ) : setup_postdata( $post );
                     ?>
      <div class="blog-item">
         <div class="row">
            <div class="col-xs-12 col-sm-2 col-md-2 text-center">
               <div class="entry-meta">
                  <span id="publish_date"><?= the_modified_date() ?></span>
                  <span><i class="fa fa-user"></i>  <?php the_author() ?></span>
                  <!-- <span><i class="fa fa-comment"></i> <a href="blog-item.html#comments">2 Comments</a></span>
                     <span><i class="fa fa-heart"></i><a href="#">56 Likes</a></span> -->
               </div>
            </div>

            <div class="col-xs-12 col-sm-10 col-md-9 blog-content">
               <h2><a href="<?= get_permalink() ?>"><?= get_the_title() ?></a></h2>
               <h3><?= the_excerpt() ?></h3>
               <p class="small">
                 <?php the_time('F jS, Y') ?> &nbsp;|&nbsp;
                 <!-- by <?php the_author() ?> -->
                 Published in
                 <?php the_category(', ');
                   if($post->comment_count > 0) {
                       echo ' &nbsp;|&nbsp; ';
                       comments_popup_link('', '1 Comment', '% Comments');
                   }
                 ?>
               </p>
               <a class="btn btn-primary readmore" href="<?= get_permalink() ?>">Read More <i class="fa fa-angle-right"></i></a>
            </div>
         </div>
      </div>
      <?php endforeach;
      if(empty($_POST['check_list'])){
  			$query_args = array(
  			    'post_type'  => 'post',
            'posts_per_page' => -1,
            'category' => $cats
  			);
  			$post_array = get_posts( $query_args );
  	    $totalUsers = count($post_array);
  	    $totalPages = intval($totalUsers / $number) + 1;


        $current_page = max(1, get_query_var('paged'));
        $pages = paginate_links(array(
              'base' => get_pagenum_link(1) . '%_%',
              'format' => 'page/%#%/',
              'current' => $current_page,
              'total' => $totalPages,
              'prev_next' => false,
              'type' => 'array',
              'prev_next' => TRUE,
              'prev_text' => '&larr; Previous',
              'next_text' => 'Next &rarr;',
              ));

        if (is_array($pages)) {
  					$current_page      = (get_query_var('paged')) ? get_query_var('paged') : 1;
            //$current_page = ( get_query_var('paged') == 0 ) ? 1 : get_query_var('paged');
            echo '<ul class="pagination">';
  					foreach ($pages as $i => $page) {
                    if ($current_page != 1 && $current_page == $i) {
                        echo "<li class='a'>$page</li>";
                    } else {
                        echo "<li>$page</li>";
                    }

            }
            echo '</ul>';
        }
      }
      else{

      }

			?>
    </div>
    <div class="col-md-3 side-bar-right">
      <?php if(is_active_sidebar('blog-sidebar-widgets')){ dynamic_sidebar('blog-sidebar-widgets');}?>
    </div><!-- col-md-4 -->
  </div>
</div>
<script>
$(function(){
  $(".active").prop('checked', true);
});
// $('.cat_link').on('click', function () {
//   $('.blog-checkbox').fadeIn('slow').toggle();
//  });
//  $('.page-numbers').on('click', function () {
//    $('form[name=categories_form]').submit();
//   });
</script>
<?php get_footer(); ?>
