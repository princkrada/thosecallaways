<?php

/******** THOSE CALLAWAYS *********/

/* Shortcodes */
require_once locate_template('/inc/shortcodes.php');
/* Menu */
require_once locate_template('/inc/menu.php');
/* Post Types */
require_once locate_template('/inc/post-types.php');
/* Front Template */
require_once locate_template('/inc/front-template.php');

/* CSS */
add_action('wp_enqueue_scripts', 'css_scripts_styles', 50 );
function css_scripts_styles() {
  wp_enqueue_style( 'style', get_stylesheet_directory_uri() . '/style.css');
  // wp_enqueue_style( 'bootstrap', get_stylesheet_directory_uri() . '/css/bootstrap.min.css');
  wp_enqueue_style( 'fonts', 'http://fonts.googleapis.com/css?family=Lato:100,300,400,700,400italic,700italic');
  wp_enqueue_style( 'hovereffect', get_stylesheet_directory_uri() . '/css/hovereffect.css');
  wp_enqueue_style( 'font-awesome', '//maxcdn.bootstrapcdn.com/font-awesome/4.6.1/css/font-awesome.min.css');
  wp_enqueue_style( 'animate', get_stylesheet_directory_uri() . '/css/animate.min.css');
  wp_enqueue_style( 'blog', get_stylesheet_directory_uri() . '/css/blog.css');

   /* Javascript */
  wp_enqueue_script( 'wow', '//cdnjs.cloudflare.com/ajax/libs/wow/1.1.2/wow.min.js');
  wp_enqueue_script('jquery-min','//ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js','','',false);
  wp_enqueue_script( 'bootstrap-js', '//maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js','','',false);
  wp_enqueue_script( 'waypoints', '//cdnjs.cloudflare.com/ajax/libs/waypoints/2.0.3/waypoints.min.js','','',false);
  wp_enqueue_script( 'counterup', '//cdn.jsdelivr.net/jquery.counterup/1.0/jquery.counterup.min.js','','',false);
  wp_enqueue_script( 'main', get_stylesheet_directory_uri() . '/js/main.js','','',false);
  wp_enqueue_script( 'isotope', '//cdnjs.cloudflare.com/ajax/libs/jquery.isotope/3.0.1/isotope.pkgd.min.js','','',false);
  wp_enqueue_script( 'parallax', '//cdnjs.cloudflare.com/ajax/libs/jquery-parallax/1.1.3/jquery-parallax-min.js','','',false);
  wp_enqueue_script( 'easing', '//cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.3/jquery.easing.min.js','','',false);

}

/* Upload Loago */
function themeslug_theme_customizer( $wp_customize ) {
$wp_customize->add_section( 'themeslug_logo_section' , array(
'title' => __( 'Logo', 'themeslug' ),
'priority' => 30,
'description' => 'Upload a logo',
) );
$wp_customize->add_setting( 'themeslug_logo' );
$wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'themeslug_logo', array(
'label' => __( 'Logo', 'themeslug' ),
'section' => 'themeslug_logo_section',
'settings' => 'themeslug_logo',
) ) );
}

/* Featured Image */
add_action('customize_register', 'themeslug_theme_customizer');
add_theme_support( 'post-thumbnails' );

/* Widgets */
 if (function_exists('register_sidebar')) {
      register_sidebar(array(
        'name' => 'Sidebar Widgets',
        'id'   => 'sidebar-widgets',
        'description'   => 'These are widgets for the sidebar.',
        'before_widget' => '<div id="%1$s" class="widget %2$s">',
        'after_widget'  => '</div>',
        'before_title'  => '<h2>',
        'after_title'   => '</h2>'
      ));
    }
  register_sidebar(array(
      'name' => 'Blog Sidebar Widgets',
      'id'   => 'blog-sidebar-widgets',
      'description'   => 'These are widgets for the blog sidebar.',
      'before_widget' => '<div id="%1$s" class="widget %2$s">',
      'after_widget'  => '</div>',
      'before_title'  => '<h2>',
      'after_title'   => '</h2>'
    ));
  register_sidebar(array(
      'name' => 'Books Sidebar Widgets',
      'id'   => 'books-sidebar-widgets',
      'description'   => 'These are widgets for the books pages sidebar.',
      'before_widget' => '<div id="%1$s" class="widget %2$s">',
      'after_widget'  => '</div>',
      'before_title'  => '<h2>',
      'after_title'   => '</h2>'
    ));
  register_sidebar( array(
    'name' => '1st Column Footer',
    'id' => 'footer-left',
    'description' => 'Appears in the footer area',
    'before_widget' => '<aside id="%1$s" class="widget %2$s">',
    'after_widget' => '</aside>',
    'before_title' => '',
    'after_title' => '',
  ) );

  register_sidebar( array(
    'name' => '2nd Column Footer',
    'id' => 'footer-middle',
    'description' => 'Appears in the footer area',
    'before_widget' => '<aside id="%1$s" class="widget %2$s">',
    'after_widget' => '</aside>',
    'before_title' => '',
    'after_title' => '',
  ) );

  register_sidebar( array(
    'name' => '3rd Column Footer',
    'id' => 'footer-right',
    'description' => 'Appears in the footer area',
    'before_widget' => '<aside id="%1$s" class="widget %2$s">',
    'after_widget' => '</aside>',
    'before_title' => '',
    'after_title' => '',
  ) );

/* Widget execute php */
add_filter('widget_text','execute_php',100);
function execute_php($html){
     if(strpos($html,"<"."?php")!==false){
          ob_start();
          eval("?".">".$html);
          $html=ob_get_contents();
          ob_end_clean();
     }
     return $html;
}

//Prevent Page Loads After Submission (Gravity Forms)
add_filter( 'gform_confirmation_anchor_1', '__return_true' );
add_filter( 'gform_confirmation_anchor_3', '__return_true' );

add_action('get_header', 'remove_admin_login_header');
function remove_admin_login_header() {
	remove_action('wp_head', '_admin_bar_bump_cb');
}


/* Limit Characters */
function content($limit) {
  $content = explode(' ', get_the_content(), $limit);
  if (count($content)>=$limit) {
    array_pop($content);
    $content = implode(" ",$content).'...';
  } else {
    $content = implode(" ",$content);
  }
  $content = preg_replace('/\[.+\]/','', $content);
  $content = apply_filters('the_content', $content);
  $content = str_replace(']]>', ']]&gt;', $content);
  return $content;
}

function pagination_nav() {
    global $wp_query;

    if ( $wp_query->max_num_pages > 1 ) { ?>
        <nav class="pagination" role="navigation">
            <div class="nav-previous"><?php next_posts_link( '&larr; Older posts' ); ?></div>
            <div class="nav-next"><?php previous_posts_link( 'Newer posts &rarr;' ); ?></div>
        </nav>
<?php }
}


if ( function_exists( 'add_image_size' ) ) {
    add_image_size( 'agent-thumb', 300, 9999 ); //300 pixels wide (and unlimited height)
}

function customize_wp_bootstrap_pagination($args) {
    $args['format'] = 'page/%#%/';
    $args['total'] = 5;
    $args['previous_string'] = 'previous';
    $args['next_string'] = 'next';
    $args['type'] = 'list';

    return $args;
}
add_filter('wp_bootstrap_pagination_defaults', 'customize_wp_bootstrap_pagination');

add_shortcode('latest_news', 'latest_news');
function latest_news(){
    $cat_list = array(14,76,26,71,272, 267, 17,275,54,273, 48);
    $query_args = array(
        'post_type'       => 'post',
        'posts_per_page' => 3,
        'category' => $cat_list
    );
   $columns = 1;
   global $post;
    $news_array = get_posts( $query_args );
    $count = 0;
    foreach ( $news_array as $post ) : setup_postdata( $post );
      $count++;
      $first_class = ( 1 == $count ) ? 'first' : '';
      $output .='<p><strong><a href="' . get_permalink() . '">'.get_the_title().'</a></strong></br>';
      $output .=''.get_the_date().' - '.get_the_time().'</p>';
    endforeach;
    wp_reset_postdata();
    return $output;
}

function wpse_sendmail()
{
  $to      = 'matthew@goodfellowcreative.com';
  $subject = 'Stay in Touch Form';

  $data = $_REQUEST['data'];

  date_default_timezone_set('America/New_York');
  $message = 'Date/Time: ' . date('l jS \of F Y h:i:s A') . "\r\n";

  $message .= 'Name:' . "\r\n";
  $message .= $data['name'] . "\r\n";

  $message .= 'Email Address:' . "\r\n";
  $message .= $data['email'] . "\r\n";
  /*$message .= 'Website: ' . "\r\n";*/
  /*$message .= $data['website'] . "\r\n";*/
  $headers = 'From: matthew@goodfellowcreative.com' . "\r\n" .
      'Reply-To: matthew@goodfellowcreative.com' . "\r\n" .
      'X-Mailer: PHP/' . phpversion();

  mail($to, $subject, $message, $headers);

  die();
}


add_action( 'wp_ajax_sendmail', 'wpse_sendmail' );
add_action( 'wp_ajax_nopriv_sendmail', 'wpse_sendmail' );
