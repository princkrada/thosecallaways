<!DOCTYPE html>
<html lang="en">

<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">

		<title>
				<?php
				if (function_exists('is_tag') && is_tag()) {
					single_tag_title("Tag Archive for &quot;"); echo '&quot; - '; }
				elseif (is_archive()) {
					wp_title(''); echo ' Archive - '; }
				elseif (is_search()) {
					echo 'Search for &quot;'.wp_specialchars($s).'&quot; - '; }
				elseif (is_404()) {
					echo 'Not Found - '; }
				elseif (is_page() !== is_front_page()){
					echo wp_title(''); echo (' - '); }
				if (is_front_page()) {
					bloginfo('name');}
				else {
					bloginfo('name'); }
				if ($paged>1) {
					echo ' page '. $paged; }
				?>
		</title>

		<link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
		<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
		<div class="container">
				<div class="row top">
						<div class="col-md-7 col-sm-7">
								<?php if ( get_theme_mod( 'themeslug_logo' ) ) : ?>
								 <a href="<?php bloginfo('url'); ?>"><img class="img-responsive logoimg" src='<?php echo esc_url( get_theme_mod( 'themeslug_logo' ) ); ?>' alt='<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>'></a>
								<?php endif; ?>
								<!-- <img src="<?php bloginfo('template_url'); ?>/images/logo.png" class="img-responsive logo"> -->
						</div>
						<div class="col-md-5 col-sm-5 social-top">
							<ul class="list-inline">
								<li>
									<a href="https://www.facebook.com/ThoseCallaways" class="btn-social btn-outline" target="_blank"><i class="fa fa-fw fa-facebook"></i></a>
								</li>
								<li>
									<a href="https://twitter.com/ThoseCallaways" class="btn-social btn-outline" target="_blank"><i class="fa fa-fw fa-twitter"></i></a>
								</li>
								<li>
									<a href="https://www.youtube.com/user/ThoseCallawaysVideos" class="btn-social btn-outline" target="_blank"><i class="fa fa-fw fa-youtube-play"></i></a>
								</li>
								<li>
									<a href="https://www.pinterest.com/thosecallaways/" class="btn-social btn-outline" target="_blank"><i class="fa fa-fw fa-pinterest"></i></a>
								</li>
								<li>
									<a href="https://plus.google.com/100571225885877826944" class="btn-social btn-outline" target="_blank"><i class="fa fa-fw fa-google-plus"></i></a>
								</li>
							</ul>
							<h3><a href="tel:4805965751">(480) 596 - 5751</a></h3>
						</div>
				</div>
		</div>
		<!-- Navigation -->
		<nav class="navbar navbar-inverse">
						<div class="container">
								<?php if ( get_theme_mod( 'themeslug_logo' ) ) : ?>
								 <a href="<?php bloginfo('url'); ?>"><img class="img-responsive logoimg-mobile" src='<?php bloginfo('template_url'); ?>/images/logo-mobile.png' alt='<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>'></a>
								<?php endif; ?>
								<div class="navbar-header">
										<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
												<span class="sr-only">Toggle navigation</span>
												<span class="icon-bar"></span>
												<span class="icon-bar"></span>
												<span class="icon-bar"></span>
										</button>

								</div>

								<div class="collapse navbar-collapse">
										<?php main_menu(); ?>
								</div>
						</div><!--/.container-->
				</nav><!--/nav-->
