<?php

/******** THOSE CALLAWAYS FRONTPAGE TEMPLATE*********/
add_action('admin_menu', 'theme_settings');

function theme_settings() {

  //create new top-level menu
  add_menu_page('Theme Options', 'TC Settings', 'administrator', __FILE__, 'tc_settings_page');

  //call register settings function
  add_action( 'admin_init', 'register_plugin_settings' );
}


function register_plugin_settings() {
  //register our settings
  register_setting( 'settings-group', 'title' );

}

function tc_settings_page() {
?>
<div class="wrap">
<h2>Theme Options</h2>

<form method="post" action="options.php">
    <?php settings_fields( 'settings-group' ); ?>
    <?php do_settings_sections( 'settings-group' ); ?>
    <table class="form-table">
        <tr valign="top">
        <th scope="row">Title:</th>
        <td><input type="text" name="title" value="<?php echo esc_attr( get_option('title') ); ?>"/></td>
        </tr>
    </table>
    <?php submit_button(); ?>

</form>
</div>
<?php } ?>
