<?php

// Register the Those Callaways Slider Post Type
function tc_slider() {

	$labels = array(
		'name'                  => _x( 'Those Callaways Slides', 'Post Type General Name', 'thosecallaways' ),
		'singular_name'         => _x( 'Those Callaways Slider', 'Post Type Singular Name', 'thosecallaways' ),
		'menu_name'             => __( 'Those Callaways Slider', 'thosecallaways' ),
		'name_admin_bar'        => __( 'Those Callaways Slider', 'thosecallaways' ),
		'archives'              => __( 'Item Archives', 'thosecallaways' ),
		'parent_item_colon'     => __( 'Parent Item:', 'thosecallaways' ),
		'all_items'             => __( 'All Items', 'thosecallaways' ),
		'add_new_item'          => __( 'Add New Item', 'thosecallaways' ),
		'add_new'               => __( 'Add New', 'thosecallaways' ),
		'new_item'              => __( 'New Item', 'thosecallaways' ),
		'edit_item'             => __( 'Edit Item', 'thosecallaways' ),
		'update_item'           => __( 'Update Item', 'thosecallaways' ),
		'view_item'             => __( 'View Item', 'thosecallaways' ),
		'search_items'          => __( 'Search Item', 'thosecallaways' ),
		'not_found'             => __( 'Not found', 'thosecallaways' ),
		'not_found_in_trash'    => __( 'Not found in Trash', 'thosecallaways' ),
		'featured_image'        => __( 'Featured Image', 'thosecallaways' ),
		'set_featured_image'    => __( 'Set featured image', 'thosecallaways' ),
		'remove_featured_image' => __( 'Remove featured image', 'thosecallaways' ),
		'use_featured_image'    => __( 'Use as featured image', 'thosecallaways' ),
		'insert_into_item'      => __( 'Insert into item', 'thosecallaways' ),
		'uploaded_to_this_item' => __( 'Uploaded to this item', 'thosecallaways' ),
		'items_list'            => __( 'Items list', 'thosecallaways' ),
		'items_list_navigation' => __( 'Items list navigation', 'thosecallaways' ),
		'filter_items_list'     => __( 'Filter items list', 'thosecallaways' ),
	);
	$args = array(
		'label'                 => __( 'TC Slider', 'thosecallaways' ),
		'description'           => __( 'Slides for the home page.', 'thosecallaways' ),
		'labels'                => $labels,
		'supports'              => array( 'title', 'custom-fields', ),
		'taxonomies'            => array( 'category', 'post_tag' ),
		'hierarchical'          => false,
		'public'                => true,
		'show_ui'               => true,
		'show_in_menu'          => true,
		'menu_position'         => 5,
		'show_in_admin_bar'     => true,
		'show_in_nav_menus'     => true,
		'can_export'            => true,
		'has_archive'           => false,
		'exclude_from_search'   => true,
		'publicly_queryable'    => true,
		'capability_type'       => 'page',
	);
	register_post_type( 'tc_slider', $args );

}
add_action( 'init', 'tc_slider', 0 );

function client_raves() {

  $labels = array(
    'name'                  => _x( 'Client Raves', 'Post Type General Name', 'those_clientraves' ),
    'singular_name'         => _x( 'Client Raves', 'Post Type Singular Name', 'those_clientraves' ),
    'menu_name'             => __( 'Client Raves', 'those_clientraves' ),
    'name_admin_bar'        => __( 'Client Raves', 'those_clientraves' ),
    'archives'              => __( 'Item Archives', 'those_clientraves' ),
    'parent_item_colon'     => __( 'Parent Item:', 'those_clientraves' ),
    'all_items'             => __( 'All Items', 'those_clientraves' ),
    'add_new_item'          => __( 'Add New Item', 'those_clientraves' ),
    'add_new'               => __( 'Add New', 'those_clientraves' ),
    'new_item'              => __( 'New Item', 'those_clientraves' ),
    'edit_item'             => __( 'Edit Item', 'those_clientraves' ),
    'update_item'           => __( 'Update Item', 'those_clientraves' ),
    'view_item'             => __( 'View Item', 'those_clientraves' ),
    'search_items'          => __( 'Search Item', 'those_clientraves' ),
    'not_found'             => __( 'Not found', 'those_clientraves' ),
    'not_found_in_trash'    => __( 'Not found in Trash', 'those_clientraves' ),
    'featured_image'        => __( 'Featured Image', 'those_clientraves' ),
    'set_featured_image'    => __( 'Set featured image', 'those_clientraves' ),
    'remove_featured_image' => __( 'Remove featured image', 'those_clientraves' ),
    'use_featured_image'    => __( 'Use as featured image', 'those_clientraves' ),
    'insert_into_item'      => __( 'Insert into item', 'those_clientraves' ),
    'uploaded_to_this_item' => __( 'Uploaded to this item', 'those_clientraves' ),
    'items_list'            => __( 'Items list', 'those_clientraves' ),
    'items_list_navigation' => __( 'Items list navigation', 'those_clientraves' ),
    'filter_items_list'     => __( 'Filter items list', 'those_clientraves' ),
  );
  $args = array(
    'label'                 => __( 'Client Raves', 'those_clientraves' ),
    'description'           => __( 'Client raves', 'those_clientraves' ),
    'labels'                => $labels,
    'supports'              => array( 'title', 'custom-fields', ),
    'taxonomies'            => array( 'category', 'post_tag' ),
    'hierarchical'          => false,
    'public'                => true,
    'show_ui'               => true,
    'show_in_menu'          => true,
    'menu_position'         => 5,
    'show_in_admin_bar'     => true,
    'show_in_nav_menus'     => true,
    'can_export'            => true,
    'has_archive'           => false,
    'exclude_from_search'   => true,
    'publicly_queryable'    => true,
    'capability_type'       => 'page',
  );
  register_post_type( 'client-raves', $args );

}
add_action( 'init', 'client_raves', 0 );


function industry_praise() {

  $labels = array(
    'name'                  => _x( 'Industry Praise', 'Post Type General Name', 'those_industrypraise' ),
    'singular_name'         => _x( 'Industry Praise', 'Post Type Singular Name', 'those_industrypraise' ),
    'menu_name'             => __( 'Industry Praise', 'those_industrypraise' ),
    'name_admin_bar'        => __( 'Industry Praise', 'those_industrypraise' ),
    'archives'              => __( 'Item Archives', 'those_industrypraise' ),
    'parent_item_colon'     => __( 'Parent Item:', 'those_industrypraise' ),
    'all_items'             => __( 'All Items', 'those_industrypraise' ),
    'add_new_item'          => __( 'Add New Item', 'those_industrypraise' ),
    'add_new'               => __( 'Add New', 'those_industrypraise' ),
    'new_item'              => __( 'New Item', 'those_industrypraise' ),
    'edit_item'             => __( 'Edit Item', 'those_industrypraise' ),
    'update_item'           => __( 'Update Item', 'those_industrypraise' ),
    'view_item'             => __( 'View Item', 'those_industrypraise' ),
    'search_items'          => __( 'Search Item', 'those_industrypraise' ),
    'not_found'             => __( 'Not found', 'those_industrypraise' ),
    'not_found_in_trash'    => __( 'Not found in Trash', 'those_industrypraise' ),
    'featured_image'        => __( 'Featured Image', 'those_industrypraise' ),
    'set_featured_image'    => __( 'Set featured image', 'those_industrypraise' ),
    'remove_featured_image' => __( 'Remove featured image', 'those_industrypraise' ),
    'use_featured_image'    => __( 'Use as featured image', 'those_industrypraise' ),
    'insert_into_item'      => __( 'Insert into item', 'those_industrypraise' ),
    'uploaded_to_this_item' => __( 'Uploaded to this item', 'those_industrypraise' ),
    'items_list'            => __( 'Items list', 'those_industrypraise' ),
    'items_list_navigation' => __( 'Items list navigation', 'those_industrypraise' ),
    'filter_items_list'     => __( 'Filter items list', 'those_industrypraise' ),
  );
  $args = array(
    'label'                 => __( 'Industry Praise', 'those_industrypraise' ),
    'description'           => __( 'Industry Praise', 'those_industrypraise' ),
    'labels'                => $labels,
    'supports'              => array( 'title', 'custom-fields', ),
    'taxonomies'            => array( 'category', 'post_tag' ),
    'hierarchical'          => false,
    'public'                => true,
    'show_ui'               => true,
    'show_in_menu'          => true,
    'menu_position'         => 5,
    'show_in_admin_bar'     => true,
    'show_in_nav_menus'     => true,
    'can_export'            => true,
    'has_archive'           => false,
    'exclude_from_search'   => true,
    'publicly_queryable'    => true,
    'capability_type'       => 'page',
  );
  register_post_type( 'industry-praise', $args );

}
add_action( 'init', 'industry_praise', 0 );

function reader_reviews() {

  $labels = array(
    'name'                  => _x( 'Reader Reviews', 'Post Type General Name', 'those_readerreviews' ),
    'singular_name'         => _x( 'Reader Reviews', 'Post Type Singular Name', 'those_readerreviews' ),
    'menu_name'             => __( 'Reader Reviews', 'those_readerreviews' ),
    'name_admin_bar'        => __( 'Reader Reviews', 'those_readerreviews' ),
    'archives'              => __( 'Item Archives', 'those_readerreviews' ),
    'parent_item_colon'     => __( 'Parent Item:', 'those_readerreviews' ),
    'all_items'             => __( 'All Items', 'those_readerreviews' ),
    'add_new_item'          => __( 'Add New Item', 'those_readerreviews' ),
    'add_new'               => __( 'Add New', 'those_readerreviews' ),
    'new_item'              => __( 'New Item', 'those_readerreviews' ),
    'edit_item'             => __( 'Edit Item', 'those_readerreviews' ),
    'update_item'           => __( 'Update Item', 'those_readerreviews' ),
    'view_item'             => __( 'View Item', 'those_readerreviews' ),
    'search_items'          => __( 'Search Item', 'those_readerreviews' ),
    'not_found'             => __( 'Not found', 'those_readerreviews' ),
    'not_found_in_trash'    => __( 'Not found in Trash', 'those_readerreviews' ),
    'featured_image'        => __( 'Featured Image', 'those_readerreviews' ),
    'set_featured_image'    => __( 'Set featured image', 'those_readerreviews' ),
    'remove_featured_image' => __( 'Remove featured image', 'those_readerreviews' ),
    'use_featured_image'    => __( 'Use as featured image', 'those_readerreviews' ),
    'insert_into_item'      => __( 'Insert into item', 'those_readerreviews' ),
    'uploaded_to_this_item' => __( 'Uploaded to this item', 'those_readerreviews' ),
    'items_list'            => __( 'Items list', 'those_readerreviews' ),
    'items_list_navigation' => __( 'Items list navigation', 'those_readerreviews' ),
    'filter_items_list'     => __( 'Filter items list', 'those_readerreviews' ),
  );
  $args = array(
    'label'                 => __( 'Reader Reviews', 'those_readerreviews' ),
    'description'           => __( 'Reader Reviews', 'those_readerreviews' ),
    'labels'                => $labels,
    'supports'              => array( 'title', 'custom-fields', ),
    'taxonomies'            => array( 'category', 'post_tag' ),
    'hierarchical'          => false,
    'public'                => true,
    'show_ui'               => true,
    'show_in_menu'          => true,
    'menu_position'         => 5,
    'show_in_admin_bar'     => true,
    'show_in_nav_menus'     => true,
    'can_export'            => true,
    'has_archive'           => false,
    'exclude_from_search'   => true,
    'publicly_queryable'    => true,
    'capability_type'       => 'page',
  );
  register_post_type( 'reader-reviews', $args );

}
add_action( 'init', 'reader_reviews', 0 );

function endorsements() {

  $labels = array(
    'name'                  => _x( 'Endorsements', 'Post Type General Name', 'those_endorsements' ),
    'singular_name'         => _x( 'Endorsements', 'Post Type Singular Name', 'those_endorsements' ),
    'menu_name'             => __( 'Endorsements', 'those_endorsements' ),
    'name_admin_bar'        => __( 'Endorsements', 'those_endorsements' ),
    'archives'              => __( 'Item Archives', 'those_endorsements' ),
    'parent_item_colon'     => __( 'Parent Item:', 'those_endorsements' ),
    'all_items'             => __( 'All Items', 'those_endorsements' ),
    'add_new_item'          => __( 'Add New Item', 'those_endorsements' ),
    'add_new'               => __( 'Add New', 'those_endorsements' ),
    'new_item'              => __( 'New Item', 'those_endorsements' ),
    'edit_item'             => __( 'Edit Item', 'those_endorsements' ),
    'update_item'           => __( 'Update Item', 'those_endorsements' ),
    'view_item'             => __( 'View Item', 'those_endorsements' ),
    'search_items'          => __( 'Search Item', 'those_endorsements' ),
    'not_found'             => __( 'Not found', 'those_endorsements' ),
    'not_found_in_trash'    => __( 'Not found in Trash', 'those_endorsements' ),
    'featured_image'        => __( 'Featured Image', 'those_endorsements' ),
    'set_featured_image'    => __( 'Set featured image', 'those_endorsements' ),
    'remove_featured_image' => __( 'Remove featured image', 'those_endorsements' ),
    'use_featured_image'    => __( 'Use as featured image', 'those_endorsements' ),
    'insert_into_item'      => __( 'Insert into item', 'those_endorsements' ),
    'uploaded_to_this_item' => __( 'Uploaded to this item', 'those_endorsements' ),
    'items_list'            => __( 'Items list', 'those_endorsements' ),
    'items_list_navigation' => __( 'Items list navigation', 'those_endorsements' ),
    'filter_items_list'     => __( 'Filter items list', 'those_endorsements' ),
  );
  $args = array(
    'label'                 => __( 'Endorsements', 'those_endorsements' ),
    'description'           => __( 'Endorsements', 'those_endorsements' ),
    'labels'                => $labels,
    'supports'              => array( 'title', 'custom-fields', ),
    'taxonomies'            => array( 'category', 'post_tag' ),
    'hierarchical'          => false,
    'public'                => true,
    'show_ui'               => true,
    'show_in_menu'          => true,
    'menu_position'         => 5,
    'show_in_admin_bar'     => true,
    'show_in_nav_menus'     => true,
    'can_export'            => true,
    'has_archive'           => false,
    'exclude_from_search'   => true,
    'publicly_queryable'    => true,
    'capability_type'       => 'page',
  );
  register_post_type( 'endorsements', $args );

}
add_action( 'init', 'endorsements', 0 );
