<?php

/******** THOSE CALLAWAYS *********/

/* Shortcode */
function seoUrl($string) {
    //Lower case everything
    $string = strtolower($string);
    //Make alphanumeric (removes all other characters)
    $string = preg_replace("/[^a-z0-9_\s-]/", "", $string);
    //Clean up multiple dashes or whitespaces
    $string = preg_replace("/[\s-]+/", " ", $string);
    //Convert whitespaces and underscore to dash
    $string = preg_replace("/[\s_]/", "-", $string);
    return $string;
}
add_shortcode('featured-listings', 'featured_listings');
function featured_listings(){
    $query_args = array(
        'post_type'       => 'listing',
        'status'  => 'featured',
        'posts_per_page' => '6'
    );

   $columns = 3;
   global $post;

    $listings_array = get_posts( $query_args );

    $count = 0;

    $output = '<div class="wp-listings-shortcode">';

$output .='

<select class="filters-select form-control">
  <option value="*">Show All</option>
  <option value="highToLow">Highest to Lowest</option>
  <option value="lowToHigh">Lowest to highest</option>
  <option value="numberGreaterThan1">$0 - $300,000</option>
  <option value="numberGreaterThan2">$301,000 - $500,000</option>
  <option value="numberGreaterThan3">$501,000 - $1,000,000</option>
  <option value="numberGreaterThanPlus">$1,000,000 plus</option>

</select>
<div style="clear:both"></div>
<div class="no-result"><p>No results found.</p></div>
';
$output .='<div class="grid">';
foreach ( $listings_array as $post ) : setup_postdata( $post );

    $count = ( $count == $columns ) ? 1 : $count + 1;

    $first_class = ( 1 == $count ) ? 'first' : '';

    if($count == 1){
      $animate =  '0.3s';
    }
    elseif($count == 2){
      $animate = '0.7s';
    }
    else{
      $animate = '1.1s';
    }

    $output .='<div class="element-item col-md-4 col-sm-4 '.$test.'"><p class="number" style="display: none;">'.get_post_meta( $post->ID, '_listing_price', true ).'</p><p class="numbers" style="display: none;">'.get_post_meta( $post->ID, '_listing_price', true ).'</p>';
    $output .= '<div class="listing-wrap animated wow fadeInDown ' . get_column_class($columns) . ' ' . $first_class . '" data-wow-delay="'.$animate.'">

    <div class="thumbnail"><div class="listing-widget-thumb">
      <a href="http://thosecallaways.idxbroker.com/idx/details/listing/a012/'.get_post_meta($post->ID, '_listing_mls',true).'/'.seoUrl(get_post_meta($post->ID, '_listing_address', true).'-'.get_post_meta($post->ID, '_listing_city', true).'-'.get_post_meta($post->ID, '_listing_country', true).'-'.get_post_meta($post->ID, '_listing_zip', true)).'" class="listing-image-link">
        <div class="hovereffect">' . get_the_post_thumbnail( $post->ID, 'listings' ) . '
          <div class="overlay"><h2>View Property</h2></div>
        </div>
      </a>';
      if ( '' != wp_listings_get_status() ) {
          $output .= '<span class="listing-status ' . strtolower(str_replace(' ', '-', wp_listings_get_status())) . '">' . wp_listings_get_status() . '</span>';
      }

      $output .= '<div class="listing-thumb-meta">';

      if ( '' != get_post_meta( $post->ID, '_listing_text', true ) ) {
          $output .= '<span class="listing-text">' . get_post_meta( $post->ID, '_listing_text', true ) . '</span>';
      } elseif ( '' != wp_listings_get_property_types() ) {
        //  $output .= '<span class="listing-property-type">' . wp_listings_get_property_types() . '</span>';
      }

      // if ( '' != get_post_meta( $post->ID, '_listing_price', true ) ) {
      // $output .= '<span class="listing-price">$' . number_format(get_post_meta( $post->ID, '_listing_price', true )) . '</span>';
      // }
      $listingPrice = get_post_meta($post->ID, '_listing_price', true);
      if(strpos($listingPrice, '$') !== false){
        $output .= '<span class="listing-price">'. get_post_meta( $post->ID, '_listing_price', true ). '</span>';
      }
      else{
        $output .= '<span class="listing-price">$'. number_format(get_post_meta( $post->ID, '_listing_price', true )) . '</span>';

      }


      $output .= '</div><!-- .listing-thumb-meta --></div><!-- .listing-widget-thumb -->';

      if ( '' != get_post_meta( $post->ID, '_listing_open_house', true ) ) {
          $output .= '<span class="listing-open-house">' . __( "Open House", 'wp_listings' ) . ': ' . get_post_meta( $post->ID, '_listing_open_house', true ) . '</span>';
      }

      $output .= '<div class="listing-widget-details">';
      $output .= '<p class="listing-address"><span>' . wp_listings_get_address() . '</span><br />';
      $output .= '<span>' . wp_listings_get_city() . ', ' . wp_listings_get_state() . ' ' . get_post_meta( $post->ID, '_listing_zip', true ) . '</span></p>';

      $output .= sprintf('<a href="http://thosecallaways.idxbroker.com/idx/details/listing/a012/'.get_post_meta($post->ID, '_listing_mls',true).'/'.seoUrl(get_post_meta($post->ID, '_listing_address', true).'-'.get_post_meta($post->ID, '_listing_city', true).'-'.get_post_meta($post->ID, '_listing_country', true).'-'.get_post_meta($post->ID, '_listing_zip', true)).'" class="view-this-property-a"><div class="view-this-property">VIEW THIS PROPERTY</div></a>');

    $output .= '</div><!-- .listing-widget-details --></div></div><!-- .listing-wrap -->
    ';
    $output .='</div>';

    endforeach;
    $output .='</div><!-- GRID -->';

    $output .= '</div><!-- .wp-listings-shortcode -->';
    wp_reset_postdata();
    return $output;

}

/* Search */
add_shortcode('search-listings', 'search_listings');
function search_listings(){

    $type = sanitize_text_field($_POST['type']);
    $status = sanitize_text_field($_POST['status']);
    $city = sanitize_text_field($_POST['city']);
    $mls = sanitize_text_field($_POST['mls']);
    $min_beds = sanitize_text_field($_POST['bedrooms']);
    $min_baths = sanitize_text_field($_POST['bathrooms']);
    $min_price = sanitize_text_field($_POST['min_price_']);
    $max_price = sanitize_text_field($_POST['max_price_']);

    if(empty($min_price)){
      $min_price_query = 0;
    }
    else{
      $min_price_query = $min_price;
    }
    if(empty($max_price)){
      $max_price_query = 6000000;
    }
    else{
      $max_price_query = $max_price;
    }

    $query_args = array(
        'post_type'     => 'listing',
        'posts_per_page' => -1,
        'status'     => $status,
        'property-types' => $type,
        'meta_query' => array(
        'relation' => 'AND', /* <-- here */
          array(
              'key' => '_listing_city',
              'value' => $city,
              'compare' => 'LIKE'
          ),
          array(
              'key' => '_listing_mls',
              'value' => $mls,
              'compare' => 'LIKE'
          ),
          // array(
          //     'key' => '_listing_price',
          //     'value' => array($min_price_query, $max_price_query),
          //     'compare' => 'BETWEEN'
          // ),
          array(
              'key'       => '_listing_bedrooms',
              'value'     => $min_beds,
              'compare'   => 'LIKE'
          ),
          array(
              'key'       => '_listing_bathrooms',
              'value'     => $min_baths,
              'compare'   => 'LIKE'
          )
    )

    );

    $columns = 3;
    global $post;



    $listings_array = get_posts( $query_args );

    if( empty($listings_array)){
      $output = '';
      $output .= '<h2>No listings found. Try another search.</h2>';
      $output .='<form method="get" class="listing-search-form IDX-searchForm IDX-searchForm-search  IDX-activept-sfr" action="http://thosecallaways.idxbroker.com/idx/results/listings">
   <div class="row">
      <div class="form-group col-sm-6">
         <input type="text" class="form-control"  placeholder="MLS #" name="csv_listingID">
      </div>
      <div class="form-group col-sm-6">
         <!-- <input type="text" class="form-control"  placeholder="City" name="city"> -->
         <select id="IDX-city" name="city[]" class="IDX-select IDX-cczSelect form-control">
                        <option value="">Select City</option>
                        <option value="362">Aguila</option>
                        <option value="54011">Ahwatukee</option>
                        <option value="392">Ajo</option>
                        <option value="808">Alpine</option>
                        <option value="1014">Amado</option>
                        <option value="1314">Anthem</option>
                        <option value="1358">Apache Junction</option>
                        <option value="1565">Arivaca</option>
                        <option value="1567">Arizona City</option>
                        <option value="1587">Arlington</option>
                        <option value="1751">Ash Fork</option>
                        <option value="2203">Avondale</option>
                        <option value="2291">Bagdad</option>
                        <option value="3417">Bellemont</option>
                        <option value="3641">Benson</option>
                        <option value="4191">Bisbee</option>
                        <option value="4246">Black Canyon City</option>
                        <option value="5108">Bouse</option>
                        <option value="5147">Bowie</option>
                        <option value="6143">Buckeye</option>
                        <option value="6295">Bullhead City</option>
                        <option value="6978">Camp Verde</option>
                        <option value="7279">Carefree</option>
                        <option value="7538">Casa Grande</option>
                        <option value="7719">Cave Creek</option>
                        <option value="8166">Chandler</option>
                        <option value="8656">Chino Valley</option>
                        <option value="8800">Circle City</option>
                        <option value="8925">Clarkdale</option>
                        <option value="9026">Clay Springs</option>
                        <option value="9033">Claypool</option>
                        <option value="9218">Clifton</option>
                        <option value="9475">Cochise</option>
                        <option value="9973">Concho</option>
                        <option value="10039">Congress</option>
                        <option value="10154">Coolidge</option>
                        <option value="10270">Cordes Lakes</option>
                        <option value="10350">Cornville</option>
                        <option value="10487">Cottonwood</option>
                        <option value="11013">Crown King</option>
                        <option value="11562">Dateland</option>
                        <option value="12152">Desert Hills</option>
                        <option value="12205">Dewey</option>
                        <option value="12617">Douglas</option>
                        <option value="12715">Dragoon</option>
                        <option value="12943">Duncan</option>
                        <option value="13145">Eagar</option>
                        <option value="14073">El Mirage</option>
                        <option value="14184">Elfrida</option>
                        <option value="14186">Elgin</option>
                        <option value="14507">Eloy</option>
                        <option value="16096">Flagstaff</option>
                        <option value="16207">Florence</option>
                        <option value="16409">Forest Lakes</option>
                        <option value="16590">Fort McDowell</option>
                        <option value="16601">Fort Mohave</option>
                        <option value="16746">Fountain Hills</option>
                        <option value="16974">Fredonia</option>
                        <option value="17868">Gila Bend</option>
                        <option value="17872">Gilbert</option>
                        <option value="18135">Glendale</option>
                        <option value="18253">Globe</option>
                        <option value="18313">Gold Canyon</option>
                        <option value="18350">Golden Valley</option>
                        <option value="18463">Goodyear</option>
                        <option value="19037">Green Valley</option>
                        <option value="19092">Greenehaven</option>
                        <option value="19240">Greer</option>
                        <option value="19417">Guadalupe</option>
                        <option value="19604">Hackberry</option>
                        <option value="20018">Happy Jack</option>
                        <option value="20678">Heber</option>
                        <option value="20906">Hereford</option>
                        <option value="21490">Holbrook</option>
                        <option value="22042">Huachuca City</option>
                        <option value="22159">Humboldt</option>
                        <option value="23537">Joseph City</option>
                        <option value="23788">Kearny</option>
                        <option value="24281">Kingman</option>
                        <option value="24421">Kirkland</option>
                        <option value="25093">Lake Havasu City</option>
                        <option value="25298">Lakeside</option>
                        <option value="25757">Laveen</option>
                        <option value="26685">Litchfield Park</option>
                        <option value="28074">Mammoth</option>
                        <option value="28402">Marana</option>
                        <option value="28489">Maricopa</option>
                        <option value="28996">Mayer</option>
                        <option value="29411">McNeal</option>
                        <option value="29476">Meadview</option>
                        <option value="29835">Mesa</option>
                        <option value="29919">Miami</option>
                        <option value="30656">Mobile</option>
                        <option value="30704">Mohave Valley</option>
                        <option value="31203">Mormon Lake</option>
                        <option value="31257">Morristown</option>
                        <option value="31800">Munds Park</option>
                        <option value="32649">New River</option>
                        <option value="33083">Nogales</option>
                        <option value="33820">Nutrioso</option>
                        <option value="34587">Oracle</option>
                        <option value="34741">Oro Valley</option>
                        <option value="35">Other</option>
                        <option value="12">Other</option>
                        <option value="4">Other</option>
                        <option value="34969">Overgaard</option>
                        <option value="35284">Palo Verde</option>
                        <option value="35382">Paradise Valley</option>
                        <option value="35524">Parker</option>
                        <option value="35564">Parks</option>
                        <option value="35721">Patagonia</option>
                        <option value="35794">Paulden</option>
                        <option value="35857">Payson</option>
                        <option value="35876">Peach Springs</option>
                        <option value="35894">Pearce</option>
                        <option value="35960">Peeples Valley</option>
                        <option value="36139">Peoria</option>
                        <option value="36412">Phoenix</option>
                        <option value="36426">Picacho</option>
                        <option value="36559">Pima</option>
                        <option value="36569">Pine</option>
                        <option value="36673">Pinedale</option>
                        <option value="36704">Pinetop</option>
                        <option value="37490">Portal</option>
                        <option value="37787">Prescott</option>
                        <option value="37793">Prescott Valley</option>
                        <option value="38173">Quartzsite</option>
                        <option value="38191">Queen Creek</option>
                        <option value="38192">Queen Valley</option>
                        <option value="38723">Red Rock</option>
                        <option value="39404">Rimrock</option>
                        <option value="39465">Rio Rico</option>
                        <option value="39467">Rio Verde</option>
                        <option value="40027">Roll</option>
                        <option value="40096">Roosevelt</option>
                        <option value="40728">Saddlebrooke</option>
                        <option value="40741">Safford</option>
                        <option value="40766">Sahuarita</option>
                        <option value="40837">Saint David</option>
                        <option value="41133">Salome</option>
                        <option value="41266">San Manuel</option>
                        <option value="41298">San Simon</option>
                        <option value="54010">San Tan Valley</option>
                        <option value="41348">Sanders</option>
                        <option value="41877">Scottsdale</option>
                        <option value="42096">Sedona</option>
                        <option value="42130">Seligman</option>
                        <option value="42814">Show Low</option>
                        <option value="42836">Shumway</option>
                        <option value="42894">Sierra Vista</option>
                        <option value="43133">Skull Valley</option>
                        <option value="43365">Snowflake</option>
                        <option value="44208">Spring Valley</option>
                        <option value="44246">Springerville</option>
                        <option value="56748">St Johns</option>
                        <option value="44407">Stanfield</option>
                        <option value="57240">Star Valley</option>
                        <option value="44993">Strawberry</option>
                        <option value="45293">Sun City</option>
                        <option value="45300">Sun City West</option>
                        <option value="45301">Sun Lakes</option>
                        <option value="45304">Sun Valley</option>
                        <option value="45409">Superior</option>
                        <option value="45433">Surprise</option>
                        <option value="45696">Tacna</option>
                        <option value="45906">Taylor</option>
                        <option value="45998">Tempe</option>
                        <option value="46132">Thatcher</option>
                        <option value="46587">Tolleson</option>
                        <option value="46600">Toltec</option>
                        <option value="46618">Tombstone</option>
                        <option value="46639">Tonopah</option>
                        <option value="46642">Tonto Basin</option>
                        <option value="46670">Topock</option>
                        <option value="46705">Tortilla Flat</option>
                        <option value="47040">Truxton</option>
                        <option value="47051">Tubac</option>
                        <option value="47065">Tucson</option>
                        <option value="48570">Vail</option>
                        <option value="48666">Valley Farms</option>
                        <option value="48941">Vernon</option>
                        <option value="49532">Waddell</option>
                        <option value="50575">Wellton</option>
                        <option value="50595">Wenden</option>
                        <option value="51536">White Mountain Lake</option>
                        <option value="51735">Wickenburg</option>
                        <option value="51765">Wikieup</option>
                        <option value="51863">Willcox</option>
                        <option value="51873">Williams</option>
                        <option value="52243">Winkelman</option>
                        <option value="52281">Winslow</option>
                        <option value="52384">Wittmann</option>
                        <option value="52874">Yarnell</option>
                        <option value="52979">Young</option>
                        <option value="53004">Youngtown</option>
                        <option value="53015">Yucca</option>
                        <option value="53024">Yuma</option>
                     </select>
      </div>
   </div>
   <div class="row">
      <div class="form-group col-sm-6">
         <select id="IDX-pt" name="pt" class="IDX-select IDX-searchType-m form-control" autocomplete="off">
            <option value="">Select Type</option>
            <option value="sfr">Single Family Residential</option>
            <option value="rnt">Rentals</option>
            <option value="ld">Lots and Land</option>
            <option value="com">Commercial</option>
            <option value="mfr">Multiple Dwellings</option>
         </select>
         <!-- <select id="IDX-pt" name="pt" class="IDX-select IDX-searchType-m form-control" autocomplete="off">
            <option value="sfr">Single Family Residential</option>
            <option value="rnt">Rentals</option>
            <option value="ld">Lots and Land</option>
            <option value="com">Commercial</option>
            <option value="mfr">Multiple Dwellings</option>
         </select> -->
      </div>
      <div class="form-group col-sm-6">
         <select id="IDX-a_propStatus" name="a_propStatus[]" class="IDX-select form-control">
            <option value="" selected>Status</option>
            <option value="Active">Active</option>
            <!-- <option value="Featured">Featured</option>
            <option value="For Rent">For Rent</option>
            <option value="New">New</option>
            <option value="Pending">Pending</option>
            <option value="Reduced">Reduced</option>
            <option value="Sold">Sold</option> -->
         </select>
      </div>
   </div>
   <div class="row" style="margin-top: 20px;">
      <div class="form-group col-sm-2">
         <!-- <input type="text" class="form-control"  placeholder="Bedrooms" name="bd"> -->
          <select id="IDX-bd" name="bd" class="IDX-select form-control" autocomplete="off">
             <option value="">Bedrooms</option>
             <option value="0">Studio</option>
             <option value="1">1+</option>
             <option value="2">2+</option>
             <option value="3">3+</option>
             <option value="4">4+</option>
             <option value="5">5+</option>
             <option value="6">6+</option>
             <option value="7">7+</option>
             <option value="8">8+</option>
             <option value="9">9+</option>
             <option value="10">10+</option>
          </select>
      </div>
      <div class="form-group col-sm-2">
         <!-- <input type="text" class="form-control"  placeholder="Bathrooms" name="tb"> -->
         <select id="IDX-tb" name="tb" class="IDX-select form-control" autocomplete="off">
             <option value="">Bathrooms</option>
             <option value="1">1+</option>
             <option value="2">2+</option>
             <option value="3">3+</option>
             <option value="4">4+</option>
             <option value="5">5+</option>
             <option value="6">6+</option>
             <option value="7">7+</option>
             <option value="8">8+</option>
             <option value="9">9+</option>
             <option value="10">10+</option>
          </select>
      </div>
      <div class="form-group col-sm-2">
         <input type="text" class="form-control"  placeholder="Minimum Price" name="lp">
      </div>
      <div class="form-group col-sm-2">
         <input type="text" class="form-control"  placeholder="Maximum Price" name="hp">
      </div>
      <div class="form-group col-sm-2">
      </div>
   </div>
   <!-- <button type="submit" id="IDX-formSubmit" class="btn-tc btn btn-default IDX-formBtn IDX-formSubmit">SUBMIT</button> -->
   <button type="submit" id="IDX-formSubmit-bottom" class="IDX-formBtn IDX-formSubmit btn-tc btn btn-default">SEARCH</button>
   <a href="http://thosecallaways.idxbroker.com/idx/searchbycity" class="btn-tc btn btn-default">OR SEARCH BY CITY</a>
</form>';
      return $output;
    }
    else{
      $count = 0;

      $output = '<div class="wp-listings-shortcode">';

      $output .='

      <select class="filters-select form-control">
        <option value="*">Show All</option>
        <option value="highToLow">Highest to Lowest</option>
        <option value="lowToHigh">Lowest to highest</option>
        <option value="numberGreaterThan1">$0 - $300,000</option>
        <option value="numberGreaterThan2">$301,000 - $500,000</option>
        <option value="numberGreaterThan3">$501,000 - $1,000,000</option>
        <option value="numberGreaterThanPlus">$1,000,000 plus</option>

      </select>
      <div style="clear:both"></div>
      <div class="no-result"><p>No results found.</p></div>
      ';
      $output .='<div class="grid">';
      foreach ( $listings_array as $post ) : setup_postdata( $post );

      $count = ( $count == $columns ) ? 1 : $count + 1;

      $first_class = ( 1 == $count ) ? 'first' : '';

      if($count == 1){
        $animate =  '0.3s';
      }
      elseif($count == 2){
        $animate = '0.7s';
      }
      else{
        $animate = '1.1s';
      }

      $output .='<div class="element-item col-md-4 col-sm-4 '.$test.'"><p class="number" style="display: none;">'.get_post_meta( $post->ID, '_listing_price', true ).'</p><p class="numbers" style="display: none;">'.get_post_meta( $post->ID, '_listing_price', true ).'</p>';
      $output .= '<div class="listing-wrap ' . get_column_class($columns) . ' ' . $first_class . '" data-wow-delay="'.$animate.'">

      <div class="thumbnail"><div class="listing-widget-thumb">
        <a href="http://thosecallaways.idxbroker.com/idx/details/listing/a012/'.get_post_meta($post->ID, '_listing_mls',true).'/'.seoUrl(get_post_meta($post->ID, '_listing_address', true).'-'.get_post_meta($post->ID, '_listing_city', true).'-'.get_post_meta($post->ID, '_listing_country', true).'-'.get_post_meta($post->ID, '_listing_zip', true)).'" class="listing-image-link">
          <div class="hovereffect">' . get_the_post_thumbnail( $post->ID, 'listings' ) . '
            <div class="overlay"><h2>View Property</h2></div>
          </div>
        </a>';
        if ( '' != wp_listings_get_status() ) {
            $output .= '<span class="listing-status ' . strtolower(str_replace(' ', '-', wp_listings_get_status())) . '">' . wp_listings_get_status() . '</span>';
        }

        $output .= '<div class="listing-thumb-meta">';

        if ( '' != get_post_meta( $post->ID, '_listing_text', true ) ) {
            $output .= '<span class="listing-text">' . get_post_meta( $post->ID, '_listing_text', true ) . '</span>';
        } elseif ( '' != wp_listings_get_property_types() ) {
          //  $output .= '<span class="listing-property-type">' . wp_listings_get_property_types() . '</span>';
        }

        // if ( '' != get_post_meta( $post->ID, '_listing_price', true ) ) {
        // $output .= '<span class="listing-price">$' .number_format(get_post_meta( $post->ID, '_listing_price', true )). '</span>';
        // }
        $listingPrice = get_post_meta($post->ID, '_listing_price', true);
        if(strpos($listingPrice, '$') !== false){
          $output .= '<span class="listing-price">'. get_post_meta( $post->ID, '_listing_price', true ). '</span>';
        }
        else{
          $output .= '<span class="listing-price">$'. number_format(get_post_meta( $post->ID, '_listing_price', true )) . '</span>';

        }

        $output .= '</div><!-- .listing-thumb-meta --></div><!-- .listing-widget-thumb -->';

        if ( '' != get_post_meta( $post->ID, '_listing_open_house', true ) ) {
            $output .= '<span class="listing-open-house">' . __( "Open House", 'wp_listings' ) . ': ' . get_post_meta( $post->ID, '_listing_open_house', true ) . '</span>';
        }

        $output .= '<div class="listing-widget-details">';
        $output .= '<p class="listing-address"><span>' . wp_listings_get_address() . '</span><br />';
        $output .= '<span>' . wp_listings_get_city() . ', ' . wp_listings_get_state() . ' ' . get_post_meta( $post->ID, '_listing_zip', true ) . '</span></p>';

        $output .= sprintf('<a href="http://thosecallaways.idxbroker.com/idx/details/listing/a012/'.get_post_meta($post->ID, '_listing_mls',true).'/'.seoUrl(get_post_meta($post->ID, '_listing_address', true).'-'.get_post_meta($post->ID, '_listing_city', true).'-'.get_post_meta($post->ID, '_listing_country', true).'-'.get_post_meta($post->ID, '_listing_zip', true)).'" class="view-this-property-a"><div class="view-this-property">VIEW THIS PROPERTY</div></a>');

      $output .= '</div><!-- .listing-widget-details --></div></div><!-- .listing-wrap -->
      ';
      $output .='</div>';

      endforeach;
      $output .='</div><!-- GRID -->';

      $output .= '</div><!-- .wp-listings-shortcode -->';
      wp_reset_postdata();
      return $output;
    }





}

add_shortcode('sticky-listing', 'sticky_listing');
function sticky_listing(){
  $query_args = array(
      'post_type' => 'listing',
      'status'  => 'featured',
      'posts_per_page' => '1'
  );
 global $post;
 $listings_array = get_posts( $query_args );

 foreach($listings_array as $listing){
  $output = '<p><span>'.$listing->post_title.'</span></p>';
  $output .= '<p><span>'.get_post_meta($listing->ID, _listing_city, true).', '.get_post_meta($listing->ID, _listing_state, true).'</span></p>';
  $output .= '<p><span>'.get_post_meta($listing->ID, _listing_price, true).'</span></p>';
  }
  return $output;
}

function render_callaways_profiles()
{
	// check if the repeater field has rows of data
	if( have_rows('agentstaff') ):

		$count = 0;
		$rowcount = 0;

		echo '<div class="row">';
	 	// loop through the rows of data
	    while ( have_rows('agentstaff') ) : the_row();

	    	$image = get_sub_field('profile_picture');
	    	$count++;

	    	$size = get_sub_field('width');
			?>

			<div class="wow fadeInDown animated profile-block col-sm-6 col-md-<?=$size;?>">
				<a type="button"  data-toggle="modal" data-target="#profile<?=$count;?>">
          <img src="<?=$image['url'];?>"/>
        </a>
				<a type="button" class="profile-name" data-toggle="modal" data-target="#profile<?=$count;?>">
					<?=get_sub_field('full_name');?>
				</a>
			</div>
			<div class="modal fade" tabindex="-1" role="dialog" id="profile<?=$count;?>">
			  <div class="modal-dialog modal-lg">
			    <div class="modal-content">
			      <div class="modal-header profile-header">
			        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			        <h4 class="modal-title"><?=get_sub_field('full_name');?> - <?=get_sub_field('years_worked');?> Years</h4>
			      </div>
			      <div class="modal-body profile-body">
			      	<div class="col-md-5 text-center">
			      		<img style="max-width:100%;" src="<?=$image['url'];?>"/>
  							<ul class="list-inline">
  								<li>
  									<a href="https://www.facebook.com/ThoseCallaways" class="btn-social btn-outline" target="_blank"><i class="fa fa-fw fa-facebook"></i></a>
  								</li>
  								<li>
  									<a href="https://twitter.com/ThoseCallaways" class="btn-social btn-outline" target="_blank"><i class="fa fa-fw fa-twitter"></i></a>
  								</li>
  								<li>
  									<a href="https://www.youtube.com/user/ThoseCallawaysVideos" class="btn-social btn-outline" target="_blank"><i class="fa fa-fw fa-youtube-play"></i></a>
  								</li>
  								<li>
  									<a href="https://www.pinterest.com/thosecallaways/" class="btn-social btn-outline" target="_blank"><i class="fa fa-fw fa-pinterest"></i></a>
  								</li>
  								<li>
  									<a href="https://plus.google.com/100571225885877826944" class="btn-social btn-outline" target="_blank"><i class="fa fa-fw fa-google-plus"></i></a>
  								</li>
  							</ul>
                <h4 class="profile-position"><?=get_sub_field('position');?></h4>
  							<h3><a href="tel:4805965751">(480) 596 - 5751</a></h3>
  							<a href="http://thosecallaways.idxbroker.com/idx/results/listings" class="btn btn-default">View Listings</a>
                <a href="mailto:email@thosecallaways.com" class="btn btn-default">Email <?=get_sub_field('full_name');?> Now</a>
			      	</div>
			        <div class="col-md-7">
                <div class="biotext">
			        	<?=get_sub_field('bio');?>
                </div>
			        </div>
			      </div>
			      <div class="modal-footer profile-footer">
			        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
			      </div>
			    </div><!-- /.modal-content -->
			  </div><!-- /.modal-dialog -->
			</div><!-- /.modal -->

	    <?php
	    endwhile;
	    echo '</div>';

	else :

	    // no rows found

	endif;
}
add_shortcode('callaways_profiles', 'render_callaways_profiles');

add_shortcode('testimonials', 'testimonials');
function testimonials(){

  ?>
  <h3>Author Endorsements</h3>
  <div class="col-md-3">
    <img src="http://goodfellowcreative.com/preview/thosecallaways/wp-content/uploads/2016/07/typewriter-w-open-quote-1.jpg" width="150">

  </div>
  <div class="col-md-9">
    <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
    <!-- Indicators -->
    <!-- Wrapper for slides -->
      <div class="carousel-inner" role="listbox">
        <?php
        $query_args = array(
          'post_type'       => 'endorsements',
          'order' => asc,
        );

        global $post;
        $count = 0;
        $book_blog = get_posts( $query_args );
        $count = 0;
        foreach($book_blog as $post) : setup_postdata($post);
        $count++;
        $active = ( 1 == $count ) ? 'active' : '';
         ?>

          <div class="item <?=$active?>">
            <div class="carousel-caption">
            <h4><?= $post->post_title?></h4>
            <p><?=content(50)?></p>
            <p><a href="<?= get_post_meta($post, the_permalink(), true)?>">Read more</a></p>
            </div>
          </div>

          <?php
           endforeach;
           wp_reset_postdata();
            ?>
          <p class="small"><a href="<?= home_url()?>/endorsements-page">See All Endorsements>></a></p>
      </div>
    </div>
  </div>
  <div style="clear:both"></div>
  <hr>
  <h3>Reader Reviews</h3>
  <div class="col-md-3">
    <img src="http://goodfellowcreative.com/preview/thosecallaways/wp-content/uploads/2016/07/3D-book-w-bookmark-open-quote.png" width="150">

  </div>
  <div class="col-md-9">
    <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
    <!-- Indicators -->
    <!-- Wrapper for slides -->
      <div class="carousel-inner" role="listbox">
        <?php
        $query_args = array(
          'post_type'       => 'reader-reviews',
          'order' => asc,
        );

        global $post;
        $count = 0;
        $book_blog = get_posts( $query_args );
        $count = 0;
        foreach($book_blog as $post) : setup_postdata($post);
        $count++;
        $active = ( 1 == $count ) ? 'active' : '';
         ?>

          <div class="item <?=$active?>">
            <div class="carousel-caption">
            <h4><?= $post->post_title?></h4>
            <p><?=content(45)?></p>
            <p><a href="<?= get_post_meta($post, the_permalink(), true)?>">Read more</a></p>
            </div>
          </div>

          <?php
           endforeach;
           wp_reset_postdata();
            ?>
          <p class="small"><a href="<?= home_url()?>/reader-reviews-page">See All Reader Reviews>></a></p>
      </div>
    </div>
  </div><!-- Reader Reviews -->

  <div style="clear:both"></div>
  <hr>
  <h3>Industry Praise</h3>
  <div class="col-md-3">
    <img src="http://goodfellowcreative.com/preview/thosecallaways/wp-content/uploads/2016/07/tc-logo-w-open-quote2.png" width="150">

  </div>
  <div class="col-md-9">
    <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
    <!-- Indicators -->
    <!-- Wrapper for slides -->
      <div class="carousel-inner" role="listbox">
        <?php
        $query_args = array(
          'post_type'       => 'industry-praise',
          'order' => asc,
        );

        global $post;
        $count = 0;
        $book_blog = get_posts( $query_args );
        $count = 0;
        foreach($book_blog as $post) : setup_postdata($post);
        $count++;
        $active = ( 1 == $count ) ? 'active' : '';
         ?>

          <div class="item <?=$active?>">
            <div class="carousel-caption">
            <h4><?= $post->post_title?></h4>
            <p><?=content(50)?></p>
            <p><a href="<?= get_post_meta($post, the_permalink(), true)?>">Read more</a></p>
            </div>
          </div>

          <?php
           endforeach;
           wp_reset_postdata();
            ?>
          <p class="small"><a href="<?= home_url()?>/industry-praise-page">See All Industry Praise>></a></p>
      </div>
    </div>
  </div><!-- Industry Praise -->

  <div style="clear:both"></div>
  <hr>
  <h3>Client Raves</h3>
  <div class="col-md-3">
    <img src="http://goodfellowcreative.com/preview/thosecallaways/wp-content/uploads/2016/07/logo-am-client-w-open-quote2.png" width="150">

  </div>
  <div class="col-md-9">
    <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
    <!-- Indicators -->
    <!-- Wrapper for slides -->
      <div class="carousel-inner" role="listbox">
        <?php
        $query_args = array(
          'post_type'       => 'client-raves',
          'order' => asc,
        );

        global $post;
        $count = 0;
        $book_blog = get_posts( $query_args );
        $count = 0;
        foreach($book_blog as $post) : setup_postdata($post);
        $count++;
        $active = ( 1 == $count ) ? 'active' : '';
         ?>

          <div class="item <?=$active?>">
            <div class="carousel-caption">
            <h4><?= $post->post_title?></h4>
            <p><?=content(50)?></p>
            <p><a href="<?= get_post_meta($post, the_permalink(), true)?>">Read more</a></p>
            </div>
          </div>

          <?php
           endforeach;
           wp_reset_postdata();
            ?>
          <p class="small"><a href="<?= home_url()?>/client-raves-page">See All Client Raves>></a></p>
      </div>
    </div>
  </div><!-- Client Raves -->
  <hr>
  <?

}

add_shortcode('testi-post-type', 'testi_posttype');
function testi_posttype($atts , $content = null){
  global $post;
  extract(shortcode_atts(array(
        "post-type" => 'post',
    ), $atts));


  $post_type = $atts['post-type'];

  $number     = 5;
  if ( get_query_var('paged') ) {
    $paged = get_query_var('paged');
  } else if ( get_query_var('page') ) {
      $paged = get_query_var('page');
  } else {
      $paged = 1;
  }

  $query_args = array(
    'post_type'       => $post_type,
    'posts_per_page' => $number,
    'paged' => $paged,

  );

  $client_raves = get_posts( $query_args );
  $total_post = count($client_raves);
  $total_query = count($query_args);
  $total_pages = intval($total_post / $number) + 1;
  ?>
  <?php
  foreach($client_raves as $post) : setup_postdata($post);

  ?>

  <h3><?= $post->post_title ?></h3>
  <p><?= content(60) ?></p>
  <p><a href="<?= get_post_meta($post, the_permalink(), true)?>">Read More</a></p>
  <?php
  endforeach;

  $query = array(
    'post_type' => $post_type,
    'posts_per_page' => -1,
  );
  $get_post = get_posts( $query );
  $totalPosts = count($get_post);
  $totalPages = intval($totalPosts / $number) + 1;

  if ($totalPosts > $total_query) {

    $current_page = max(1, get_query_var('paged'));
    $pages = paginate_links(array(
          'base' => get_pagenum_link(1) . '%_%',
          'format' => 'page/%#%/',
          'current' => $current_page,
          'total' => $totalPages,
          'prev_next' => false,
          'type' => 'array',
          'prev_next' => TRUE,
          'prev_text' => '&larr; Previous',
          'next_text' => 'Next &rarr;',
          ));

    if (is_array($pages)) {
        $current_page      = (get_query_var('paged')) ? get_query_var('paged') : 1;
        //$current_page = ( get_query_var('paged') == 0 ) ? 1 : get_query_var('paged');
        echo '<ul class="pagination">';
        foreach ($pages as $i => $page) {
                if ($current_page != 1 && $current_page == $i) {
                    echo "<li class='a'>$page</li>";
                } else {
                    echo "<li>$page</li>";
                }

        }
        echo '</ul>';
    }

  }

}

add_shortcode('book-blog', 'book_blog');
function book_blog(){
  global $post;

  $number     = 5;
  if ( get_query_var('paged') ) {
    $paged = get_query_var('paged');
  } else if ( get_query_var('page') ) {
      $paged = get_query_var('page');
  } else {
      $paged = 1;
  }

  $query_args = array(
    'post_type'       => 'post',
    'category' => 1037,
    'posts_per_page' => $number,
    'paged' => $paged,

  );

  $book_blog = get_posts( $query_args );
  $total_post = count($book_blog);
  $total_query = count($query_args);
  $total_pages = intval($total_post / $number) + 1;
  ?>
  <?php
  foreach($book_blog as $post) : setup_postdata($post);

  ?>
  <div class="blog">
    <div class="blog-item">
     <div class="row">
        <div class="col-xs-12 col-sm-2 col-md-2 text-center">
           <div class="entry-meta">
              <span id="publish_date"><?= the_modified_date() ?></span>
              <span><i class="fa fa-user"></i>  <?php the_author() ?></span>
              <!-- <span><i class="fa fa-comment"></i> <a href="blog-item.html#comments">2 Comments</a></span>
                 <span><i class="fa fa-heart"></i><a href="#">56 Likes</a></span> -->
           </div>
        </div>

        <div class="col-xs-12 col-sm-10 col-md-9 blog-content">
           <h2><a href="<?= get_permalink() ?>"><?= get_the_title() ?></a></h2>
           <h3><?= the_excerpt() ?></h3>
           <p class="small">
             <?php the_time('F jS, Y') ?> &nbsp;|&nbsp;
             <!-- by <?php the_author() ?> -->
             Published in
             <?php the_category(', ');
               if($post->comment_count > 0) {
                   echo ' &nbsp;|&nbsp; ';
                   comments_popup_link('', '1 Comment', '% Comments');
               }
             ?>
           </p>
           <a class="btn btn-primary readmore" href="<?= get_permalink() ?>">Read More <i class="fa fa-angle-right"></i></a>
        </div>
     </div>
    </div>
  </div>

  <hr>
  <?php
  endforeach;

  $query = array(
    'post_type' => 'post',
    'posts_per_page' => -1,
    'category' => 1037,
  );
  $get_post = get_posts( $query );
  $totalPosts = count($get_post);
  $totalPages = intval($totalPosts / $number) + 1;

  if ($totalPosts > $total_query) {

    $current_page = max(1, get_query_var('paged'));
    $pages = paginate_links(array(
          'base' => get_pagenum_link(1) . '%_%',
          'format' => 'page/%#%/',
          'current' => $current_page,
          'total' => $totalPages,
          'prev_next' => false,
          'type' => 'array',
          'prev_next' => TRUE,
          'prev_text' => '&larr; Previous',
          'next_text' => 'Next &rarr;',
          ));

    if (is_array($pages)) {
        $current_page      = (get_query_var('paged')) ? get_query_var('paged') : 1;
        //$current_page = ( get_query_var('paged') == 0 ) ? 1 : get_query_var('paged');
        echo '<ul class="pagination">';
        foreach ($pages as $i => $page) {
                if ($current_page != 1 && $current_page == $i) {
                    echo "<li class='a'>$page</li>";
                } else {
                    echo "<li>$page</li>";
                }

        }
        echo '</ul>';
    }

  }

}

add_shortcode('booksblog_sidebar', 'booksblog_sidebar');
function booksblog_sidebar(){
  ?>
  <h3><i class="fa fa-calendar-check-o" aria-hidden="true"></i> &nbsp;&nbsp;Upcoming Events</h3>
  <?php
  $query_event = array(
    'post_type'       => 'event',
    'order' => asc,
    'posts_per_page' => '3'
  );

  global $post;

  $events = get_posts( $query_event );

  foreach($events as $post) : setup_postdata($post);
   ?>
   <p><i class="fa fa-check" aria-hidden="true"></i>&nbsp;&nbsp;<a href="<?= get_post_meta($post, the_permalink(), TRUE) ?>"><?= $post->post_title ?></a></p>
   <?php
    endforeach;
    wp_reset_postdata();
   ?>
   <h3><i class="fa fa-calendar-o" aria-hidden="true"></i> &nbsp;&nbsp;Host An Event</h3>
    <!-- <div id="gallery" style="margin-top:12px; margin-bottom: 25px;">
    <div data-type="youtube"
       data-videoid="mUuVu0L1Gdw"
       data-title="Host an Event"
       data-description="Host an Event"></div>
    </div> -->
    <p>Host an Event! Audiences rave at hearing Joseph and JoAnn tell their stories live.</p>
    <ul>
      <li>Be Excited</li>
      <li>Be Moved</li>
      <li>Be Changed</li>
    </ul>
    <h3><i class="fa fa-envelope-o" aria-hidden="true"></i> &nbsp;&nbsp;Newsletter Sign Up</h3>
    <p><a href="<?= home_url()?>/books/stay-updated-clients-first/"><span style="color: red;">Click here</span></a> to subscribe our Clients First email list and receive updates on event schedules, news, tips on putting the Clients First philosophy to work for you, and more. Opt-out anytime. </p>
<?php
}

add_shortcode('quicksearch', 'render_quicksearch');

function render_quicksearch()
{

  $output = '';

$output = '<section id="search-filters">
            <!-- SEARCH BY CITY -->
            <div class="container-fluid">
              <div class="row">
                <div class="col-lg-12">
                  <form method="post" class="listing-search-form" action="'.get_site_url().'/all-listings">
                    <div class="row">
                      <div class="form-group col-sm-6">
                        <input type="text" class="form-control"  placeholder="MLS #" name="mls">
                      </div>
                      <div class="form-group col-sm-6">
                        <input type="text" class="form-control"  placeholder="City" name="city">
                      </div>
                    </div>
                    <div class="row">
                      <div class="form-group col-sm-6">
                        <select  class="form-control" name="type">
                          <option value="" disabled selected>Type</option>
                          <option value="Lots and Land">Lots and Land</option>
                          <option value="Commercial">Commercial</option>
                          <option value="Condo">Condo</option>
                          <option value="Residential">Residential</option>
                          <option value="Townhome">Townhome</option>
                        </select>
                      </div>
                      <div class="form-group col-sm-6">
                        <select  class="form-control" name="status">
                          <option value="" selected>Status</option>
                          <option value="Active">Active</option>
                          <option value="Featured">Featured</option>
                          <option value="For Rent">For Rent</option>
                          <option value="New">New</option>
                          <option value="Pending">Pending</option>
                          <option value="Reduced">Reduced</option>
                          <option value="Sold">Sold</option>
                        </select>
                      </div>
                    </div>
                    <div class="row" style="margin-top: 20px;">
                      <div class="form-group col-sm-2">
                        <input type="text" class="form-control"  placeholder="Bedrooms" name="bedrooms">
                      </div>
                      <div class="form-group col-sm-2">
                        <input type="text" class="form-control"  placeholder="Bathrooms" name="bathrooms">
                      </div>
                      <div class="form-group col-sm-2">
                        <input type="text" class="form-control"  placeholder="Minimum Price" name="min_price_">
                      </div>
                      <div class="form-group col-sm-2">
                        <input type="text" class="form-control"  placeholder="Maximum Price" name="max_price_">
                      </div>
                      <div class="form-group col-sm-2">
                      </div>
                    </div>
                    <button type="submit" class="btn-tc btn btn-default">SUBMIT</button>
                    <a href="http://thosecallaways.idxbroker.com/idx/searchbycity" class="btn-tc btn btn-default">OR SEARCH BY CITY</a>
                  </form>
                </div>
              </div>
            </div>
            <!-- /.container -->
          </section>';

          return $output;

}
