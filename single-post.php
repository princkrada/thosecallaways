<?php
/**
 *
 */

get_header();


    global $post;
?>
<div style="height: 195px; background-image: url('<?= get_stylesheet_directory_uri()?>/images/3-full.jpg');background-size: cover;background-repeat: no-repeat; width: 100%;background-position:50% 10">
  <div class="ft-overlay">
    <div class="container">
      <span class="listing-detail-title"><h2>BLOG</h2></span>
    </div>
  </div>
</div>

<div class="container pages-container">
  <div class="row" style="margin-top: 30px;">
    <div class="col-md-9">
      <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
      <div class="page-content">
        <div class="breadcrumbs" typeof="BreadcrumbList" vocab="http://schema.org/">
            <?php if(function_exists('bcn_display'))
            {
                bcn_display();
            }?>
        </div>
        <h2><?= the_title() ?></h2>
        <p><?= the_content() ?></p>
      </div>
      <?php
      endwhile;
      else:
      endif;

      if ( comments_open() || get_comments_number() ) {
        comments_template();
      }

      ?>
    </div>
    <div class="col-md-3 side-bar-right">
      <?php if(is_active_sidebar('blog-sidebar-widgets')){ dynamic_sidebar('blog-sidebar-widgets');}?>
    </div><!-- col-md-4 -->
  </div>
</div>

<?php get_footer(); ?>
