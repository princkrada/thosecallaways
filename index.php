<?php
/**
 *
 */

get_header();


    global $post;
?>
<div style="height: 195px; background-image: url('<?= get_stylesheet_directory_uri()?>/images/3-full.jpg');background-size: cover;background-repeat: no-repeat; width: 100%;background-position:50% 10">
  <div class="ft-overlay">
    <div class="container">
    </div>
  </div>
</div>

<div class="container" style="margin-top: 20px;">
  <div class="row" style="margin-top: 30px;">
    <div class="col-md-9">
      <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
      <div class="page-content">
        <h2><?= the_title() ?></h2>
        <p><?= the_content() ?></p>
      </div>
      <?php
      endwhile;
      else:
      endif;
      ?>
    </div>
    <div class="col-md-3 side-bar-right">
      <?php if(is_active_sidebar('sidebar-widgets')){ dynamic_sidebar('sidebar-widgets');}?>
    </div><!-- col-md-4 -->
  </div>
</div>

<?php get_footer(); ?>
