<?php
/**
 * Single Listing Template: Single Listing
 */

get_header(); ?>

<link rel='stylesheet' href='<?php bloginfo('template_url'); ?>/inc/3rd-party/unitegallery/css/unite-gallery.css' type='text/css' />
<link rel='stylesheet'        href='<?php bloginfo('template_url'); ?>/inc/3rd-party/unitegallery/themes/default/ug-theme-default.css' type='text/css' />

<div style="height: 195px; background-image: url('<?php bloginfo('template_url'); ?>/images/3-full.jpg');background-size: cover;background-repeat: no-repeat; width: 100%;background-position:50% 10;">
  <div class="ft-overlay">
    <div class="container">
      <span class="listing-detail-title">Home/Listing/<?= the_title() ?></span>

    </div>
  </div>
</div>

<!-- <img style="width: 100%; height: auto" src="<?php bloginfo('template_url'); ?>/assets/img/listing-image.png" /> -->

<div class="container listing-profile">
  <div class="row">
    <div class="col-md-12">
      <div class="col-md-6 no-pad">
        <h2><?=  get_post_meta( $post->ID, '_listing_address', true ).', '.get_post_meta( $post->ID, '_listing_city', true ).', '.get_post_meta($post->ID, '_listing_state', true) ?> </h2>


      </div>
      <div class="col-md-6 no-pad text-right">
        <ul class="list-inline" style="text-align: right">
          <li>
              <a href="#" class="btn-share btn-outline" target="_blank"><i class="fa fa-print" aria-hidden="true"></i></a>
          </li>
          <li>
              <a href="https://www.facebook.com/kwviprealty" class="btn-share btn-outline" target="_blank"><i class="fa fa-fw fa-facebook"></i></a>
          </li>
          <li>
              <a href="https://twitter.com/kwviprealty" class="btn-share btn-outline" target="_blank"><i class="fa fa-fw fa-twitter"></i></a>
          </li>
          <li>
              <a href="https://www.youtube.com/watch?list=PLD53882AC2A116F70&v=EBNonO7GgLc" class="btn-share btn-outline" target="_blank"><i class="fa fa-fw fa-youtube-play"></i></a>
          </li>
          <li>
              <a href="https://www.youtube.com/watch?list=PLD53882AC2A116F70&v=EBNonO7GgLc" class="btn-share btn-outline" target="_blank"><i class="fa fa-fw fa-pinterest"></i></a>
          </li>
          <li>
              <a href="https://www.youtube.com/watch?list=PLD53882AC2A116F70&v=EBNonO7GgLc" class="btn-share btn-outline" target="_blank"><i class="fa fa-fw fa-google-plus"></i></a>
          </li>
        </ul>
        <h2>$ <?= get_post_meta( $post->ID, '_listing_price', true ) ?> </h2>
        <h4><?= get_post_meta( $post->ID, '_listing_sqft', true ) !=  null ? '<span>'.get_post_meta( $post->ID, '_listing_sqft', true ).'/Sqft</span>': '' ?></h4>
      </div>
      <div style="clear:both"></div>
      <?php

      $listing_gallery = get_post_meta($post->ID, '_listing_gallery');
      if($listing_gallery != null){

      }
      else{
        //$gallery = get_post_gallery_images($post->ID);
        //var_dump($listing_gallery);
        if($listing_gallery != null){
          echo '<div id="gallery" style="display:none;">';
          foreach ($listing_gallery as $imgurl) {
            $src = (string) reset(simplexml_import_dom(DOMDocument::loadHTML($imgurl))->xpath("//img/@src"));
            ?>
            <img src="<?= $src ?>" data-image="<?= $src ?>" class="img-gal">
            <?php
          }
          echo '</div>';
        }
        else
        {

        }
      }

      ?>
    </div>
  </div>
  <div class="row">
    <div class="col-md-9">

      <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>

      <div class="c-listing-detail">

        <p>Description</p>
        <hr>
        <h3>$ <?= get_post_meta( $post->ID, '_listing_price', true ) ?></h3>
        <p><?= the_content() ?></p>
      </div>
      <?php
      endwhile;
      else:
      endif;
      ?>
      <?php
      $pattern = '<tr class="wp_listings%s"><td class="col-md-5">%s</td><td>%s</td></tr>';
      $details_instance = new WP_Listings();
       ?>
      <div class="c-listing-detail">
        <p class="c-title">Property Details</p>
        <table class="table table-bordered">
          <tbody>
            <tr>
              <td>MLS #</td>
              <td><?= get_post_meta($post->ID, '_listing_mls', true) ?></td>
            </tr>
            <?php

            foreach ( (array) $details_instance->property_details['col2'] as $label => $key ) {
              $detail_value = esc_html( get_post_meta($post->ID, $key, true) );
              if (! empty( $detail_value ) ) :
                printf( $pattern, $key, esc_html( $label ), $detail_value );
              endif;
            }

            foreach ( (array) $details_instance->extended_property_details['col1'] as $label => $key ) {
              $detail_value = esc_html( get_post_meta($post->ID, $key, true) );
              if (! empty( $detail_value ) ) :
                printf( $pattern, $key, esc_html( $label ), $detail_value );
              endif;
            }

             foreach ( (array) $details_instance->extended_property_details['col2'] as $label => $key ) {
              $detail_value = esc_html( get_post_meta($post->ID, $key, true) );
              if (! empty( $detail_value ) ) :
                printf( $pattern, $key, esc_html( $label ), $detail_value );
              endif;
            }

            ?>
          </tbody>
        </table>
      </div>
      <?php  if ( get_post_meta( $post->ID, '_listing_home_sum', true) != '' || get_post_meta( $post->ID, '_listing_kitchen_sum', true) != '' || get_post_meta( $post->ID, '_listing_living_room', true) != '' || get_post_meta( $post->ID, '_listing_master_suite', true) != '') { ?>
      <div class="additional-features">
        <p class="c-title">Additional Features</p>
        <table class="table table-bordered">
        <tbody>
          <tr>
            <td class="col-md-5"><?php _e("Home Summary", 'wp-listings'); ?></td>
            <td class="col-md-5"><?= get_post_meta( $post->ID, '_listing_home_sum', true) ?></td>
          </tr>
          <tr>
            <td class="col-md-5"><?php _e("Kitchen Summary", 'wp-listings'); ?></td>
            <td class="col-md-5"><?= get_post_meta( $post->ID, '_listing_kitchen_sum', true) ?></td>
          </tr>
          <tr>
            <td class="col-md-5"><?php _e("Living Room", 'wp-listings'); ?></td>
            <td class="col-md-5"><?= get_post_meta( $post->ID, '_listing_living_room', true) ?></td>
          </tr>
          <tr>
            <td class="col-md-5"><?php _e("Master Suite", 'wp-listings'); ?></td>
            <td class="col-md-5"><?= get_post_meta( $post->ID, '_listing_master_suite', true) ?></td>
          </tr>
        </tbody>
        </table>
      </div><!-- .additional-features -->
      <?php
        } ?>

      <?php
      if (get_post_meta( $post->ID, '_listing_map', true) != '') {
        echo '<div id="listing-map"><h3>Location Map</h3>';
        echo do_shortcode(get_post_meta( $post->ID, '_listing_map', true) );
        echo '</div><!-- .listing-map -->';
      }
      elseif(get_post_meta( $post->ID, '_listing_latitude', true) && get_post_meta( $post->ID, '_listing_longitude', true) && get_post_meta( $post->ID, '_listing_automap', true) == 'y') {

        $map_info_content = sprintf('<p style="font-size: 14px; margin-bottom: 0;">%s<br />%s %s, %s</p>', get_post_meta( $post->ID, '_listing_address', true), get_post_meta( $post->ID, '_listing_city', true), get_post_meta( $post->ID, '_listing_state', true), get_post_meta( $post->ID, '_listing_zip', true));

        echo '<script src="https://maps.googleapis.com/maps/api/js"></script>
        <script>
          function initialize() {
            var mapCanvas = document.getElementById(\'map-canvas\');
            var myLatLng = new google.maps.LatLng(' . get_post_meta( $post->ID, '_listing_latitude', true) . ', ' . get_post_meta( $post->ID, '_listing_longitude', true) . ')
            var mapOptions = {
              center: myLatLng,
              zoom: 14,
              mapTypeId: google.maps.MapTypeId.ROADMAP
              }

              var marker = new google.maps.Marker({
                position: myLatLng,
                icon: \'//s3.amazonaws.com/ae-plugins/wp-listings/images/active.png\'
            });

            var infoContent = \' ' . $map_info_content . ' \';

            var infowindow = new google.maps.InfoWindow({
              content: infoContent
            });

              var map = new google.maps.Map(mapCanvas, mapOptions);

              marker.setMap(map);

              infowindow.open(map, marker);
          }
          google.maps.event.addDomListener(window, \'load\', initialize);
        </script>
        ';
        echo '<div id="listing-map"><h3>Location Map</h3><div id="map-canvas" style="width: 100%; height: 350px;"></div></div><!-- .listing-map -->';
      }
    ?>

    <?php
      if (function_exists('_p2p_init') && function_exists('agent_profiles_init') ) {
        echo'<div id="listing-agent">
        <div class="connected-agents">';
        aeprofiles_connected_agents_markup();
        echo '</div></div><!-- .listing-agent -->';
      } elseif (function_exists('_p2p_init') && function_exists('impress_agents_init') ) {
        echo'<div id="listing-agent">
        <div class="connected-agents">';
        impa_connected_agents_markup();
        echo '</div></div><!-- .listing-agent -->';
      }
    ?>




    </div>
    <div class="col-md-3">
      <?php if(is_active_sidebar('sidebar-widgets')){ dynamic_sidebar('sidebar-widgets');}?>
    </div><!-- col-md-4 -->
  </div>
</div>



<?php get_footer(); ?>
    <script type="text/javascript">

    jQuery(document).ready(function(){



      jQuery("#gallery").unitegallery({
        theme_enable_text_panel: false,
        strip_space_between_thumbs:15,
        thumb_width:120,
        thumb_height:90,
        thumb_image_overlay_type: "blur",
        slider_control_zoom:false,
        slider_enable_zoom_panel: false,
        gallery_width:1170,
        gallery_height:620,
      });

    });

    </script>

    <script type='text/javascript' src='<?php bloginfo('template_url'); ?>/inc/3rd-party/unitegallery/js/unitegallery.min.js'></script>
    <script type='text/javascript' src='<?php bloginfo('template_url'); ?>/inc/3rd-party/unitegallery/themes/default/ug-theme-default.js'></script>
