<?php
/**
 * The Template for displaying all single products
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/single-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see 	    https://docs.woocommerce.com/document/template-structure/
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     1.6.4
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

get_header( 'shop' ); ?>
<link rel='stylesheet' href='<?php bloginfo('template_url'); ?>/inc/3rd-party/unitegallery/css/unite-gallery.css' type='text/css' />
<link rel='stylesheet'        href='<?php bloginfo('template_url'); ?>/inc/3rd-party/unitegallery/themes/default/ug-theme-default.css' type='text/css' />


<div class="container pages-container book-page" style="margin-top:97px;">
	<?php
		/**
		 * woocommerce_before_main_content hook.
		 *
		 * @hooked woocommerce_output_content_wrapper - 10 (outputs opening divs for the content)
		 * @hooked woocommerce_breadcrumb - 20
		 */
		do_action( 'woocommerce_before_main_content' );
	?>

		<?php while ( have_posts() ) : the_post(); ?>

			<?php wc_get_template_part( 'content', 'single-product' ); ?>

		<?php endwhile; // end of the loop. ?>

	<?php
		/**
		 * woocommerce_after_main_content hook.
		 *
		 * @hooked woocommerce_output_content_wrapper_end - 10 (outputs closing divs for the content)
		 */
		do_action( 'woocommerce_after_main_content' );
	?>

  <div class="col-md-3 side-bar-right">
    <?php if(is_active_sidebar('books-sidebar-widgets')){ dynamic_sidebar('books-sidebar-widgets');}?>
  </div><!-- col-md-4 -->
</div>
</div>
<script>
jQuery(function(){
  jQuery("#gallery").unitegallery({
    gallery_theme: "slider",
    gallery_carousel:false,
    gallery_control_thumbs_mousewheel:false,
    slider_enable_bullets: false,
    slider_enable_arrows: false
});
});

</script>
<script type='text/javascript' src='<?php bloginfo('template_url'); ?>/inc/3rd-party/unitegallery/js/unitegallery.min.js'></script>
<script type='text/javascript' src='<?php bloginfo('template_url'); ?>/inc/3rd-party/unitegallery/themes/default/ug-theme-default.js'></script>
<script src='<?php bloginfo('template_url'); ?>/inc/3rd-party/unitegallery/themes/slider/ug-theme-slider.js' type='text/javascript'></script>


<?php get_footer( 'shop' ); ?>
