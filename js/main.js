jQuery(function($) {'use strict',

	$('.counter').counterUp({
      delay: 20,
      time: 1000
  });

	//Initiat WOW JS
	new WOW().init();

	//goto top
	$('.gototop').click(function(event) {
		event.preventDefault();
		$('html, body').animate({
			scrollTop: $("body").offset().top
		}, 500);
	});

});


(function($) {
 "use strict"

 // Accordion Toggle Items
   var iconOpen = 'fa fa-minus',
        iconClose = 'fa fa-plus';

    $(document).on('show.bs.collapse hide.bs.collapse', '.accordion', function (e) {
        var $target = $(e.target)
          $target.siblings('.accordion-heading')
          .find('em').toggleClass(iconOpen + ' ' + iconClose);
          if(e.type == 'show')
              $target.prev('.accordion-heading').find('.accordion-toggle').addClass('active');
          if(e.type == 'hide')
              $(this).find('.accordion-toggle').not($target).removeClass('active');
    });

})(jQuery);

$(function(){
	$('#myCarousel').carousel({
		interval: 8000
	});
	$('#testimonial').carousel({
		interval: 2000
	});
	$('.accordion-heading').find('br').remove();
	//
	// jQuery.scrollSpeed(100, 800);

	$window = $(window);
	if( $window .width() > 800){
		$('.latest-blogs').parallax("100%", 0.4); // Counter
		$('.latest-blogs').each(function(index, element) {
			$(this).addClass('fixed');
		});

		$('.buttons-parallax').parallax("100%", 0.2); // Counter
		$('.buttons-parallax').each(function(index, element) {
			$(this).addClass('fixed');
		});
	}
	if( $window .width() < 768){
		$('.dropdown').on('mouseenter mouseleave click tap', function() {
		  $(this).toggleClass("open");
		});
	}
	//$(function(){
	$('.sec-one').wrapAll('<div class="col-md-8" />');
	$('.sec-two').wrapAll('<div class="col-md-4" />');
	//});

	// init Isotope
	var $grid = $('.grid').isotope({
		itemSelector: '.element-item',
		layoutMode: 'fitRows',
		getSortData: {
			number: '.number parseInt',
			numbers: '.numbers parseInt',
		}
	});
	// filter functions
	var filterFns = {
		// show if number is greater than 50
		numberGreaterThan1: function() {
			var number = $(this).find('.number').text();
			//var number = numbers.replace(/[^0-9]/g, "");
			return parseInt( number, 10 ) < 300000;
		},
		numberGreaterThan2: function() {
			var number = $(this).find('.number').text();
			//console.log(numbers);
			//var number = numbers.replace(/[^0-9]/g, "");
			return (parseInt( number, 10 ) > 301000) && (parseInt( number, 10 ) < 500000) ;
		},
		numberGreaterThan3: function() {
			var number = $(this).find('.number').text();
			//var number = numbers.replace(/[^0-9]/g, "");
			return (parseInt( number, 10 ) > 501000) && (parseInt( number, 10 ) < 1000000) ;
		},
		numberGreaterThanPlus: function() {
			var number = $(this).find('.number').text();
		//	var number = numbers.replace(/[^0-9]/g, "");
			return parseInt( number, 10 ) > 1000000;
		},
		// lowToHigh: function(){
		// 	var numbers = $(this).find('.number').text();
		//   //console.log(sortByValues);

		// 	return numbers;
		// }
		// show if name ends with -ium
		// ium: function() {
		//   var name = $(this).find('.name').text();
		//   return name.match( /ium$/ );
		// }
	};
	// bind filter on select change
	$('.filters-select').on( 'change', function() {
		// get filter value from option value
		var filterValue = this.value;
		// if(filterValue == 'numberGreaterThan1' || filterValue == '*'){
		// 	// use filterFn if matches value
		// 	filterValue = filterFns[ filterValue ] || filterValue;
		// 	//console.log(filterValue);
		// 	$grid.isotope({ filter: filterValue });
		//
		// }
		if(filterValue == 'highToLow'){
			filterValue = filterFns[ filterValue ] || filterValue;
			//console.log(filterValue);
			$grid.isotope({ sortBy: 'number', sortAscending: false });

		}
		else if(filterValue == 'lowToHigh'){
			filterValue = filterFns[ filterValue ] || filterValue;
			//console.log(filterValue);
			$grid.isotope({ sortBy: 'number', sortAscending: true });
		}
		else{
			filterValue = filterFns[ filterValue ] || filterValue;
			console.log(filterValue);
			$grid.isotope({ filter: filterValue });

			console.log($("div.element-item:visible").length)
			setTimeout(function(){
				console.log($("div.element-item:visible").length)
			  	if ( $("div.element-item:visible").length == 0){
						$('.no-result').show();

				}
				else{
					// NOT EMPTY
					$('.no-result').hide();
				}
			}, 700);

		}
		return false;

	});


});

$(function(){

	$("#contact-send").submit(function(e){

	    e.preventDefault();
			var name = $('#contact-send input[name="name"]').val();
	    var email = $('#contact-send input[name="email"]').val();

	    var data = { 'action':'sendmail', 'data': {name:name, email:email}};

	    $.post('http://goodfellowcreative.com/preview/thosecallaways/wp-admin/admin-ajax.php', data, function(response) {
	        $('#stay-in-touch .stay-in-touch-p').hide();
					$('#stay-in-touch .stay-in-touch-thankyou').fadeIn('slow');
	    });

	});
})

