<?php
/**
 *The template for displaying 404 pages (Not Found)
 */

get_header();


    global $post;
    $src = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), array( 5600,1000 ), false, '' );
    if ( has_post_thumbnail() ) {
        ?>
        <div style="background: url(<?php echo $src[0]; ?> );" class="page-featured-image"></div>
        <?php
    }
    else {
      ?>
      <div style="height: 195px; background-image: url('<?= get_stylesheet_directory_uri()?>/images/3-full.jpg');background-size: cover;background-repeat: no-repeat; width: 100%;background-position:50% 10">
        <div class="ft-overlay">
          <div class="container">
            <span class="listing-detail-title">404 Page Not Found</h2></span>
          </div>
        </div>
      </div>
      <?php
    }
?>
<div class="container pages-container">
  <div class="row">
    <div class="col-md-9">
      <div class="page-content">
        <h2>This is somewhat embarrassing, isn’t it?</h2>
        <p>It looks like nothing was found at this location. Maybe try a search?</p>
        <div class="center-block text-center">
          <?php get_search_form(); ?>
        </div>
      </div>
    </div>
    <div class="col-md-3 side-bar-right">
      <?php if(is_active_sidebar('sidebar-widgets')){ dynamic_sidebar('sidebar-widgets');}?>
    </div><!-- col-md-4 -->
  </div>
</div>

<?php get_footer(); ?>
