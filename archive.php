<?php get_header(); ?>
<div style="height: 195px; background-image: url('<?php bloginfo('template_url'); ?>/images/3-full.jpg');background-size: cover;background-repeat: no-repeat; width: 100%;background-position:50% 10;">
  <div class="ft-overlay">
    <div class="container">
      <span class="listing-detail-title"><h2>Archives</h2></span>

    </div>
  </div>
</div>
<div class="container pages-container">
   <div class="row">
      <div class="col-md-9">
         <?php if (have_posts()) : ?>
         <div class="page-content" >
            <?php $post = $posts[0]; // Hack. Set $post so that the_date() works. ?>
            <?php /* If this is a category archive */ if (is_category()) { ?>
            <h2>Archive for the &#8216;<?php single_cat_title(); ?>&#8217; Category</h2>
            <?php /* If this is a tag archive */ } elseif( is_tag() ) { ?>
            <h2>Posts Tagged &#8216;<?php single_tag_title(); ?>&#8217;</h2>
            <?php /* If this is a daily archive */ } elseif (is_day()) { ?>
            <h2>Archive for <?php the_time('F jS, Y'); ?></h2>
            <?php /* If this is a monthly archive */ } elseif (is_month()) { ?>
            <h2>Archive for <?php the_time('F, Y'); ?></h2>
            <?php /* If this is a yearly archive */ } elseif (is_year()) { ?>
            <h2>Archive for <?php the_time('Y'); ?></h2>
            <?php /* If this is an author archive */ } elseif (is_author()) { ?>
            <h2>Author Archive</h2>
            <?php /* If this is a paged archive */ } elseif (isset($_GET['paged']) && !empty($_GET['paged'])) { ?>
            <h2>Blog Archives</h2>
            <?php } ?>
            <?php include (TEMPLATEPATH . '/inc/nav.php' ); ?>
            <?php while (have_posts()) : the_post(); ?>
            <div <?php post_class() ?>>
               <h4 id="post-<?php the_ID(); ?>"> - <a href="<?php the_permalink() ?>"><?php the_title(); ?></a></h4>
               <?php include (TEMPLATEPATH . '/inc/meta.php' ); ?>
               <!-- <div class="entry">
                  <?php the_content(); ?>
                  </div> -->
            </div>
            <?php endwhile; ?>
            <?php include (TEMPLATEPATH . '/inc/nav.php' ); ?>
            <?php else : ?>
            <h2>Nothing found</h2>
            <?php endif; ?>
         </div>
      </div>
      <div class="col-md-3">
         <?php if(is_active_sidebar('blog-sidebar-widgets')){ dynamic_sidebar('blog-sidebar-widgets');}?>
      </div>
      <!-- col-md-4 -->
   </div>
</div>
<?php get_footer(); ?>
